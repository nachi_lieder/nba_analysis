import goldsberry
import pandas as pd
pd.set_option("display.max_columns", 20)
from datetime import datetime, timedelta
from dateutil import parser
from nba_py import shotchart
from nba_py.player import get_player
from IPython.display import Image
from  nba_py.team import *
from  nba_py.game import PlayerTracking
import numpy as np
from scipy.stats import binned_statistic_2d
import seaborn as sns
# from bokeh.plotting import figure
from math import pi

import urllib
import matplotlib.pyplot as plt
from matplotlib.patches import Circle, Rectangle, Arc


Image("Find-game-ID-notes.png")

# gid = '0021500550'
from matplotlib.patches import Circle, Rectangle, Arc

def draw_court(ax=None, color='black', lw=2, outer_lines=False):
    # If an axes object isn't provided to plot onto, just get current one
    if ax is None:
        ax = plt.gca()

    # Create the various parts of an NBA basketball court

    # Create the basketball hoop
    # Diameter of a hoop is 18" so it has a radius of 9", which is a value
    # 7.5 in our coordinate system
    hoop = Circle((0, 0), radius=7.5, linewidth=lw, color=color, fill=False)

    # Create backboard
    backboard = Rectangle((-30, -7.5), 60, -1, linewidth=lw, color=color)

    # The paint
    # Create the outer box 0f the paint, width=16ft, height=19ft
    outer_box = Rectangle((-80, -47.5), 160, 190, linewidth=lw, color=color,
                          fill=False)
    # Create the inner box of the paint, widt=12ft, height=19ft
    inner_box = Rectangle((-60, -47.5), 120, 190, linewidth=lw, color=color,
                          fill=False)

    # Create free throw top arc
    top_free_throw = Arc((0, 142.5), 120, 120, theta1=0, theta2=180,
                         linewidth=lw, color=color, fill=False)
    # Create free throw bottom arc
    bottom_free_throw = Arc((0, 142.5), 120, 120, theta1=180, theta2=0,
                            linewidth=lw, color=color, linestyle='dashed')
    # Restricted Zone, it is an arc with 4ft radius from center of the hoop
    restricted = Arc((0, 0), 80, 80, theta1=0, theta2=180, linewidth=lw,
                     color=color)

    # Three point line
    # Create the side 3pt lines, they are 14ft long before they begin to arc
    corner_three_a = Rectangle((-220, -47.5), 0, 140, linewidth=lw,
                               color=color)
    corner_three_b = Rectangle((220, -47.5), 0, 140, linewidth=lw, color=color)
    # 3pt arc - center of arc will be the hoop, arc is 23'9" away from hoop
    # I just played around with the theta values until they lined up with the 
    # threes
    three_arc = Arc((0, 0), 475, 475, theta1=22, theta2=158, linewidth=lw,
                    color=color)

    # Center Court
    center_outer_arc = Arc((0, 422.5), 120, 120, theta1=180, theta2=0,
                           linewidth=lw, color=color)
    center_inner_arc = Arc((0, 422.5), 40, 40, theta1=180, theta2=0,
                           linewidth=lw, color=color)

    # List of the court elements to be plotted onto the axes
    court_elements = [hoop, backboard, outer_box, inner_box, top_free_throw,
                      bottom_free_throw, restricted, corner_three_a,
                      corner_three_b, three_arc, center_outer_arc,
                      center_inner_arc]

    if outer_lines:
        # Draw the half court line, baseline and side out bound lines
        outer_lines = Rectangle((-250, -47.5), 500, 470, linewidth=lw,
                                color=color, fill=False)
        court_elements.append(outer_lines)

    # Add the court elements onto the axes
    for element in court_elements:
        ax.add_patch(element)

    return ax

def plot_heatmap_of_made_shots(made,title):
    plt.figure(figsize=(12,11))
    plt.xlim(-300,300)
    plt.ylim(-100,500)

    heatmap = made[['LOC_X','LOC_Y']]
    heatmap['round_x'] = heatmap['LOC_X'].apply( lambda x: round(x/50.0)*50.0 )
    heatmap['round_y'] = heatmap['LOC_Y'].apply( lambda x: round(x/50.0)*50.0)
    heatmap['made']=1
    # plt.scatter(heatmap['round_x'],heatmap['round_y'])
    heatmap, xedges, yedges = np.histogram2d(heatmap['LOC_X'], heatmap['LOC_Y'])
    extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
    plt.clf()
    name = 'Reds'
    plt.imshow(heatmap.T, extent=extent, origin='lower',cmap=plt.get_cmap(name))
    draw_court(outer_lines=True, color="black")
    plt.suptitle(title)
    plt.show()





def get_team_shot_distribution_by_year(_last_year,team_id):

    if datetime.now().month > 6:
        SEASON = str(_last_year) + "-" + str(_last_year + 1)[2:]
    else:
        SEASON = str(_last_year - 1) + "-" + str(_last_year)[2:]

    # shotchart.ShotChart(team_id =1610612764 ,season =CURRENT_SEASON )
    # TeamShootingSplits('1610612764').shot_areas()
    df = pd.DataFrame([])
#     print TeamCommonRoster(team_id,season=SEASON).roster()
    players_from_that_time = TeamCommonRoster(team_id,season=SEASON).roster()['PLAYER_ID'].values
    for pid in players_from_that_time:
        x = shotchart.ShotChart(pid,season =SEASON ).shot_chart()
        df=df.append(x)
    return df
    
#https://github.com/seemethere/nba_py/blob/master/nba_py/shotchart.py

# example: https://github.com/bradleyfay/py-Goldsberry/blob/master/docs/Visualizing%20NBA%20Shots%20with%20py-Goldsberry.ipynb
def get_visual_player(first_name,last_name,date):
    
    pid = get_player(first_name, last_name)
    
    if date.month > 6:
        CURRENT_SEASON = str(date) + "-" + str(date + 1)[2:]
    else:
        CURRENT_SEASON = str(date - 1) + "-" + str(date)[2:]
    # some parameters: last_n_games=30
    x = shotchart.ShotChart(pid,season =CURRENT_SEASON ).shot_chart()
    
    made = x[x['SHOT_MADE_FLAG']==1]
    missed = x[x['SHOT_MADE_FLAG']==0]
    plot_heatmap_of_made_shots(made,first_name + ' ' + last_name  +'  2017-2018')


# plt.figure(figsize=(12,11))
# plt.scatter(made.LOC_X, made.LOC_Y,color='green')
# # plt.scatter(missed.LOC_X, missed.LOC_Y,color='red')
# # plt.scatter(made_last_year.LOC_X, made_last_year.LOC_Y,color='red')
# draw_court(outer_lines=True, color="black")
# # Descending values along the axis from left to right
# plt.xlim(-300,300)
# plt.ylim(-100,500)
# plt.suptitle('Paul George Harden Shot Distribution - 2017-2018')
# plt.show()
# 
# 
# plt.figure(figsize=(12,11))
# # plt.scatter(made.LOC_X, made.LOC_Y,color='green')
# plt.scatter(missed.LOC_X, missed.LOC_Y,color='red')
# # plt.scatter(made_last_year.LOC_X, made_last_year.LOC_Y,color='red')
# draw_court(outer_lines=True, color="black")
# # Descending values along the axis from left to right
# plt.xlim(-300,300)
# plt.ylim(-100,500)
# plt.suptitle('Paul George Shot Distribution - 2016-2017')
# plt.show()

def get_visual_team(team_id, date):
    df_10 = get_team_shot_distribution_by_year(date,'1610612745')
    made = df_10[df_10['SHOT_MADE_FLAG']==1]
    missed = df_10[df_10['SHOT_MADE_FLAG']==0]
    plot_heatmap_of_made_shots(made,'Ten Years Ago')



#############################################
##############run from here##################
#############################################
_last_year_10 = datetime.now().year -10
_this_year = datetime.now().year -1
date_now = datetime.now()
get_visual_team('1610612745',_last_year_10)
get_visual_team('1610612745',date_now)
# get_visual_player('Paul','George',date_now)    
