'''
Created on May 23, 2017

@author: fuser
'''


import itertools
import pandas as pd
from scipy import optimize
import pandas as pd
import random
import numpy as np
from kmeans import Kmeans

number_of_clusters = 10
current_status =  [None] * number_of_clusters
# current_status = [1626202    ,1626147    ,1627786    ,1627848    ,203918    ,202412    ,203086    ,201579    ,200794,    203105]
MAX_SALARY = 80000000
Iteration = 0

class Knapsack(): 
    def set_random_varibles_for_status(self,number_of_clusters,df):
        while 1:
            for cluster in range(0,number_of_clusters):
                tmp= df[df['cluster']==cluster]
                ids = tmp['PLAYER_ID'].values
                
                current_status[cluster] = random.choice(ids)
            sum=0
            for item in current_status:
                sum = sum + converter_sal[item]
            if sum<MAX_SALARY:
                return current_status


    def look_for_next_step(self,current_status ):
        dic_of_options = {}
        for cluster in range(0,number_of_clusters):
            res,player,diff  = self.better_step(current_status,cluster)
            if res == True :
                dic_of_options[cluster] = [res,int(player),float(diff)]
            
        if dic_of_options != {}:
            max = 0
            max_cluster = None
            for key in dic_of_options.iterkeys():
                if max<dic_of_options[key][2]:
                    max = dic_of_options[key][2]
                    max_cluster = key
            max_player = dic_of_options[max_cluster][1]     
            current_status[max_cluster] = int(max_player)
            current_status = self.look_for_next_step(current_status)
            
           
        return current_status
    
    def with_while_loop(self,current_status):
        number_of_loops = 0
        while 1:
            dic_of_options = {}
            for cluster in range(0,number_of_clusters):
                res,player,diff  = self.better_step(current_status,cluster)
                if res == True :
                    dic_of_options[cluster] = [res,int(player),float(diff)]
            
            if dic_of_options != {}:
                max = 0
                max_cluster = None
                for key in dic_of_options.iterkeys():
                    if max<dic_of_options[key][2]:
                        max = dic_of_options[key][2]
                        max_cluster = key
                max_player = dic_of_options[max_cluster][1]     
                current_status[max_cluster] = int(max_player)
                number_of_loops = number_of_loops +1
                
            else:
                print number_of_loops
                return current_status
                
    def better_step(self,current_status,cluster):
        tmp= df[df['cluster']==cluster].reset_index()
        tmp = tmp.sample(frac=1).reset_index(drop=True)
        current_player_index=tmp[tmp['PLAYER_ID']==int(current_status[cluster])].index[0]
        try:
            sorted_tmp=tmp.ix[current_player_index-1:current_player_index+1].sort_values('value',ascending=False)
        except:
            print 'hi'
        list_of_vals = sorted_tmp['value'].values
        for max_value in list_of_vals:
            potential_player_id = sorted_tmp[sorted_tmp['value']==max_value]['PLAYER_ID']
            if max_value>converter_val[int(current_status[cluster])] and self.check_if_player_can_be_added(current_status,potential_player_id,cluster)==True:
                return True,potential_player_id,max_value-converter_val[int(current_status[cluster])]
        return False,None,None
    
    def check_if_player_can_be_added(self,current_status,player_id,cluster):
        sum = 0
        for item in current_status:
            sum = sum + converter_sal[item]
        sum=sum-converter_sal[current_status[cluster]]+converter_sal[int(player_id)]
        if sum<=MAX_SALARY:
            return True
        else: 
            return False
            
    def create_dic_of_id_to_val(self,df):
        x = df[['PLAYER_ID','value']]
        x.index = x.PLAYER_ID
        converter = x['value'].to_dict()
        return converter
    
    def create_dic_of_id_to_salary(self,df):
        x = df[['PLAYER_ID','Salary']]
        x.index = x.PLAYER_ID
        converter = x['Salary'].to_dict()
        return converter
    
    
    def best_value(self,row,weights):
        return np.dot(list(row.fillna(0)), weights)
    
    def create_value_parameter(self):
        df = pd.read_csv('labeled_with_cluster_and_data_18.tsv',sep='\t')
#         tmp_df =  df[['AST', 'BLK', 'PTS','DREB', 'FG3A', 'FG_PCT', 'FG3_PCT', 'FGA', 'FGM', 'FTA', 'FTM',\
#                'FT_PCT', 'OREB', 'PLUS_MINUS', 'STL', 'TO']]
        tmp_df=df.filter(regex='mean|std')
#         TODO : add avg points on court vs off ,  
        tmp_df.index = df.PLAYER_ID
        weights=[1,2,2,1,1,1,1,1,1,1,1,1,1,1,1,-1]
        weights=[0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0]
        weights = [1] * len(tmp_df.columns)
        normalized_df=(tmp_df-tmp_df.mean())/tmp_df.std()
        normalized_df['value'] = normalized_df.apply(self.best_value,axis = 1, args=(weights,))
        
        with_cluster =  normalized_df.merge(df[['cluster','PLAYER_ID','Salary']],how='inner',left_index=True,right_on='PLAYER_ID')
        with_cluster[['PLAYER_ID','value','cluster','Salary']].to_csv('labeled_with_cluster_and_data_18_values.tsv')
        return True


km = Kmeans()
knapsack = Knapsack()

# create file with stats+personal attributes
grouped_with_personal = km.prepare_file_extended_v2()
km.perform_kmeans_v2(30, grouped_with_personal)
km.append_salaries_and_save_data(grouped_with_personal)
# create value parameter using weights per stats(does not include personal attributes)
knapsack.create_value_parameter()

df = pd.read_csv('labeled_with_cluster_and_data_18_values.tsv')
df =df.sort_values(['cluster','value'],ascending=[False, False])
converter_val = knapsack.create_dic_of_id_to_val(df)
converter_sal = knapsack.create_dic_of_id_to_salary(df)

for iteration in range(0,10 ):
    print '*************************************'
    current_status = knapsack.set_random_varibles_for_status(number_of_clusters,df)
    result = knapsack.with_while_loop(current_status)
#     result = look_for_next_step(current_status)
    df_names = pd.read_csv('/home/nachi/git/nba_analysis/files/labeled_18.csv')
    try:
        for item in result:
            print df_names[df_names['PLAYER_ID']== item]['PLAYER_NAME'].values[0]
            sum=0
            for item in current_status:
                sum = sum + converter_sal[item]
        print 'final cap is : ' + str(sum) 
    except:
        print 'bad id'
