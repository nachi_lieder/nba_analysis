from nba_py import game
from nba_py import player
from nba_py.constants import CURRENT_SEASON
import nba_py
import calendar
import pandas as pd
import numpy as np
from sklearn.metrics import silhouette_samples, silhouette_score
from sklearn.cluster import KMeans


HOME_DIR ='/home/nachi/git/nba_analysis/'

class Kmeans():
    def prepare_file(self):
        df = pd.read_csv(HOME_DIR + 'files/appended_file.csv' , sep='\t')
        print df.columns
        df['Date'] = pd.to_datetime(df['Date'])
        df['year'] = df['Date'].dt.year
        cut = df[df.year>=2016]
        print cut['START_POSITION']
        grouped = cut.groupby(['PLAYER_ID']).mean()
        
        
        self.names = df[['PLAYER_ID','PLAYER_NAME']].drop_duplicates()
        self.names.index = self.names.PLAYER_ID
        
        personal = pd.read_csv('files/appended_file_personal_attributes.csv',sep='\t')
        p = personal[['PERSON_ID','HEIGHT','WEIGHT']].dropna()
        p['I_HEIGHT'] = p['HEIGHT'].apply(lambda x: int(str(x)[:str(x).index("-")])*12 + int(str(x)[str(x).index("-"):]))
        p.index=p['PERSON_ID']
        grouped_with_personal = grouped.merge(p,how='inner',left_index=True,right_index=True)
        return grouped_with_personal

    def convert_minutes_to_float(self,merge):
        merge['Min_Float'] = merge[merge['MIN'].isnull()==False]['MIN'].apply(lambda x: float(x.split(':')[0]))
        merge['Sec_Float'] = merge[merge['MIN'].isnull()==False]['MIN'].apply(lambda x: float(x.split(':')[1])/60)
        merge['Total_Minutes_Float'] = merge['Min_Float'] + merge['Sec_Float']    
        return merge
    
    def prepare_box_score_with_dates(self,box_score_path):
        bx = pd.read_csv(HOME_DIR + box_score_path,sep='\t')
        bx=bx.drop_duplicates()
        bx = self.convert_minutes_to_float(bx)
        del bx['Sec_Float']
        
        df = pd.read_csv(HOME_DIR + 'files/game_info/appended_info_line_score.tsv',sep='\t')
        df['GAME_DATE_EST'] = pd.to_datetime(df['GAME_DATE_EST'])
        df=df.drop_duplicates()
        x = df[['GAME_DATE_EST','GAME_ID','TEAM_ID']]
        bx_with_date = bx.merge(x,how='inner',left_on=['GAME_ID','TEAM_ID'],right_on=['GAME_ID','TEAM_ID'])
        bx_with_date['IsStarter'] = (bx_with_date['START_POSITION'].isnull()==False).astype(int)
        return     bx_with_date
        
    def prepare_file_extended_v2(self):
        path= HOME_DIR + 'files/appended_single_game_stats/'
#         df= pd.read_csv(path+'appended.csv')
#         df= pd.read_csv(HOME_DIR + 'files/boxscores_advanced/appended_boxscores_advanced.tsv',sep='\t')
        df = self.prepare_box_score_with_dates('files/boxscores/appended_boxscores.tsv')
        print df.columns
        df['Date'] = pd.to_datetime(df['GAME_DATE_EST'])
        df['year'] = df['Date'].dt.year
#         cut = df[df.year>=2016]
        
        full_grouped = pd.DataFrame([])
        years = df.reset_index()['year'].drop_duplicates().sort_values(ascending=False).values
        methods = ['mean','std']
        for year in years:
            for method in methods:
                if method=='mean':
                    grouped = df[df['year']==year].groupby(['PLAYER_ID']).mean()
                elif method=='std':
                    grouped = df[df['year']==year].groupby(['PLAYER_ID']).std()
                else:
                    print 'we have a problem'
                grouped.columns = [str(col) + '_'+str(year)+'_'+method for col in grouped.columns]
                if len(full_grouped) ==0:
                    full_grouped = grouped
                else:
                    full_grouped = full_grouped.merge(grouped,how='left',left_index=True,right_index=True)
        
        self.names = df[['PLAYER_ID','PLAYER_NAME']].drop_duplicates()
        self.names.index = self.names.PLAYER_ID
        
        personal = pd.read_csv(HOME_DIR + 'files/appended_file_personal_attributes.csv',sep='\t')
        p = personal[['PERSON_ID','HEIGHT','WEIGHT']].dropna()
        p['I_HEIGHT'] = p['HEIGHT'].apply(lambda x: int(str(x)[:str(x).index("-")])*12 + int(str(x)[str(x).index("-"):]))
        p.index=p['PERSON_ID'].astype(int)
        grouped_with_personal = full_grouped.merge(p,how='inner',left_index=True,right_index=True)
        return grouped_with_personal

    def perform_kmeans(self,number_of_clusters,grouped_with_personal):
        number_of_clusters = 30
        for i in range(5,number_of_clusters):
            n=i
            model = KMeans(n_clusters=n, init='k-means++', max_iter=1000, n_init=1,random_state=5)
            X_train = grouped_with_personal[['AST','BLK','PTS','DREB','FG3A', 'FG_PCT','FG3_PCT','FGA', 'FGM', 'FTA', 'FTM',
                   'FT_PCT','OREB','PLUS_MINUS','STL','TO','I_HEIGHT','WEIGHT']]
            X_train=X_train.dropna()
            res = model.fit(X_train)
            silhouette_avg = silhouette_score(X_train, model.labels_)
            print("For n_clusters =", n,
                      "The average silhouette_score is :", silhouette_avg)
        
            results = pd.DataFrame(data=model.labels_, columns=['cluster'], index=X_train.index)
            labeled = results.merge(self.names,how='inner',left_index=True,right_index=True)
            print 'saving cluster values to csv'
            labeled.sort_values('cluster').to_csv('files/labeled_'+str(n)+'.csv')
        return X_train
    
    def perform_kmeans_v2(self,number_of_clusters,grouped_with_personal):
        number_of_clusters = 30
        for i in range(5,number_of_clusters):
            n=i
            model = KMeans(n_clusters=n, init='k-means++', max_iter=1000, n_init=1,random_state=5)
            
            X_train = grouped_with_personal[grouped_with_personal.filter(regex='mean|std').columns]
            X_train=X_train.dropna()
            res = model.fit(X_train)
            silhouette_avg = silhouette_score(X_train, model.labels_)
            print("For n_clusters =", n,
                      "The average silhouette_score is :", silhouette_avg)
        
            results = pd.DataFrame(data=model.labels_, columns=['cluster'], index=X_train.index)
            labeled = results.merge(self.names,how='inner',left_index=True,right_index=True)
            if 'PLAYER_NAME_x' in labeled.columns:
                labeled.rename(columns={'PLAYER_NAME_x':'PLAYER_NAME'},inplace=True) 
            print 'saving cluster values to csv'
            labeled.sort_values('cluster').to_csv('files/labeled_'+str(n)+'.csv')
        return X_train
    
    def create_salary_file(self):
        df = pd.read_csv('files/nba_salaries_raw.tsv' , sep='\t')
        df['FinalName'] =  df['Name'].apply(lambda x: x.split(',')[0])
        df['Pos'] =  df['Name'].apply(lambda x: x.split(',')[1])
        df['Salary'] = pd.to_numeric(df['Salary'].apply(lambda x: x[1:].replace(',','')))
        df.to_csv('files/nba_salaries.tsv')
        print 'dome'

    def append_salaries_and_save_data(self,X_train):
        # take the optimal cluster and append salaries
        
        labeled = pd.read_csv('files/labeled_18.csv')
        df_sal = pd.read_csv('files/nba_salaries.tsv' )
        del df_sal['Name']
        print df_sal.columns
        labeled_with_cluster = labeled.merge(df_sal,how='inner',left_on='PLAYER_NAME', right_on='FinalName')
        labeled_with_cluster_and_data = labeled_with_cluster.merge(X_train,how='inner',left_on='PLAYER_ID',right_index=True)
        labeled_with_cluster_and_data.to_csv('labeled_with_cluster_and_data_18.tsv',sep='\t',index=False)
        print labeled_with_cluster_and_data
        print 'done'
        
    
