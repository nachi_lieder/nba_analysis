from dateutil.relativedelta import relativedelta
# from nba_py import player
# from nba_py.constants import CURRENT_SEASON
# import nba_py
# from nba_py import team
# from nba_py.player import get_player
# from nba_py.constants import *
# from nba_py import game
import os
import pandas as pd
import matplotlib.pyplot as plt 
# from pandas.stats.api import ols
import statsmodels.api as sm
import numpy as np
import datetime as dt
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import confusion_matrix, f1_score
from sklearn import metrics
from sklearn.cross_validation import KFold
import datetime
from sklearn.metrics import silhouette_samples, silhouette_score
from sklearn.cluster import KMeans    
from dateutil import parser  
import math
from sklearn.linear_model import *
from sklearn.neural_network import *
from sklearn.svm import *
from sklearn.learning_curve import *  


RELATIVE_PATH = '/home/nachi/git'  
class TestsNBA(object):
    def __init__(self):
        print 'init...'
        self.labled = self.get_playerlist_per_team()
        
        print 'loading box scores...'
        self.bx =pd.read_csv('~/git/nba_analysis/files/boxscores/appended_boxscores.tsv',sep='\t')
        self.bx=self.bx.drop_duplicates()
        self.bx = self.convert_minutes_to_float(self.bx)
        
        print 'loading appended info line...'
        self.df = pd.read_csv('~/git/nba_analysis/files/game_info/appended_info_line_score.tsv',sep='\t')
        self.df['GAME_DATE_EST'] = pd.to_datetime(self.df['GAME_DATE_EST'])
        self.df=self.df.drop_duplicates()
        
    def get_minutes(self,time_str):
        if time_str == None:
            return None
        minutes, sec = time_str.split(':')
        return float(minutes) + float(float(sec)/60)
    
    def get_dummy_players():
        players = self.bx.copy()
        players['IsStarter']=0
        players.loc[players['START_POSITION'].isnull()==False,'IsStarter']=1
        p = players[players['IsStarter']==1][['GAME_ID','PLAYER_ID']]
        dummy_players = players[['GAME_ID','TEAM_ID','PLAYER_ID']].merge(pd.get_dummies(p['PLAYER_ID'], prefix='is_starter_'),
                                                               how='left',
                                                               left_index=True,
                                                               right_index=True).fillna(0)
        return dummy_players
    #################################################
    #     Description:
    #     This function tests the correlation 
    #     between the amount of time in the league and
    #     the production (plus-minus) on the court.
    #################################################
    def check_trend_plus_minus(self):
    
        bx = pd.read_csv('~/git/nba_analysis/files/boxscores/appended_boxscores.tsv',sep='\t')
        print bx.columns
        list_of_player_ids=bx['PLAYER_ID'].unique()
        bx_info = pd.read_csv('~/git/nba_analysis/files/game_info/appended_info.tsv',sep='\t')
        bx_info.drop_duplicates(inplace=True)
        bx_info['GAME_DATE_EST']=pd.to_datetime(bx_info['GAME_DATE_EST'])
        bx_info_cut = bx_info[['GAME_DATE_EST','GAME_ID','HOME_TEAM_ID','VISITOR_TEAM_ID','SEASON']]
    
        merge = bx.merge(bx_info_cut,how='inner',left_on='GAME_ID' , right_on='GAME_ID')
        
        for player_id in list_of_player_ids:
            try:
                merge_player = merge[merge['PLAYER_ID']==player_id]
    
                merge_player=merge_player[merge_player['PLUS_MINUS'].isnull()==False]
    
                X = (merge_player['GAME_DATE_EST']- dt.datetime(2014,1,1)).astype('timedelta64[D]')
                Y = merge_player['PLUS_MINUS']
                results = sm.OLS(Y,sm.add_constant(X)).fit()
                if (abs(results.tvalues[1]) > 2.5) and (len(X[X>1300])>0) and (len(X)>100) and (results.rsquared > 0.05):
                    print merge_player['PLAYER_NAME'].iloc[0]
                    print 'R Squared = ' + str(results.rsquared)
                    print 'T Stat = ' + str(results.tvalues[1])
                    plt.scatter(X,Y )
    
        #             print results.summary()
    
                    plt.scatter(X,Y)
                    plt.plot(X, X*results.params[1] + results.params[0])
                    plt.suptitle('Plus Minus Over Time')
                    plt.show()
            except :
                continue    
        
    
    def convert_minutes_to_float(self,merge):
        merge['Min_Float'] = merge[merge['MIN'].isnull()==False]['MIN'].apply(lambda x: float(x.split(':')[0]))
        merge['Sec_Float'] = merge[merge['MIN'].isnull()==False]['MIN'].apply(lambda x: float(x.split(':')[1])/60)
        merge['Total_Minutes_Float'] = merge['Min_Float'] + merge['Sec_Float']    
        return merge
    
    
    
    
    def check_one_day_rest_vs_none(self,run_reg):
        df = pd.read_csv('~/git/nba_analysis/files/game_info/appended_info_line_score.tsv',sep='\t')
        df['GAME_DATE_EST'] = pd.to_datetime(df['GAME_DATE_EST'])
        df['day_diff']=None
        appended_df_with_datediff = pd.DataFrame(columns=df.columns)
        teams = df['TEAM_ID'].unique()
        for team in teams:
            tmp_df = df[df['TEAM_ID']==team]
            tmp_df=tmp_df.sort_values('GAME_DATE_EST')
            tmp_df['day_diff'] = tmp_df['GAME_DATE_EST'].diff()
            tmp_df = tmp_df[1:]
            tmp_df['day_diff'] = tmp_df['day_diff'].dt.days
            appended_df_with_datediff = appended_df_with_datediff.append(tmp_df)
        
        if run_reg == True:
            print appended_df_with_datediff[appended_df_with_datediff['day_diff']==1].describe().ix[1].astype(str)['Win']
            print appended_df_with_datediff[appended_df_with_datediff['day_diff']==2].describe().ix[1].astype(str)['Win']
            print appended_df_with_datediff[appended_df_with_datediff['day_diff']==3].describe().ix[1].astype(str)['Win']
            print appended_df_with_datediff[appended_df_with_datediff['day_diff']==4].describe().ix[1].astype(str)['Win']    
            print appended_df_with_datediff[appended_df_with_datediff['day_diff']==5].describe().ix[1].astype(str)['Win']    
            
            
            print 'Only games in intervals of max 5 days - Linear Regression and Logistic'
            only_games_until_5 = appended_df_with_datediff[appended_df_with_datediff['day_diff']<=5]
            X = only_games_until_5['day_diff']-1
            Y = only_games_until_5['Win']
            results = sm.OLS(Y,sm.add_constant(X)).fit()
            print results.summary()
            
            results = sm.Logit(Y,sm.add_constant(X)).fit()
            print results.summary()
            
            appended_df_with_datediff.loc[appended_df_with_datediff['day_diff']>=5 ,'day_diff' ] = 5
            X = appended_df_with_datediff['day_diff']-1
            Y = appended_df_with_datediff['Win']
            results = sm.OLS(Y,sm.add_constant(X)).fit()
            print results.summary()
        
#         appended_df_with_datediff.loc[appended_df_with_datediff['day_diff']>=5 ,'day_diff' ] = 5
        appended_df_with_datediff = appended_df_with_datediff[appended_df_with_datediff['day_diff']<=5]
        return appended_df_with_datediff[['GAME_DATE_EST','TEAM_ID','GAME_ID','day_diff','Win']]
        
    def check_game_after_win(self,run_reg):
        df = pd.read_csv('~//git/nba_analysis/files/game_info/appended_info_line_score.tsv',sep='\t')
        df['GAME_DATE_EST'] = pd.to_datetime(df['GAME_DATE_EST'])
        df['Win_diff']=None
        appended_df_with_datediff = pd.DataFrame(columns=df.columns)
        teams = df['TEAM_ID'].unique()
        for team in teams:
            tmp_df = df[df['TEAM_ID']==team]
            tmp_df=tmp_df.sort_values('GAME_DATE_EST')
            tmp_df['Win_diff'] = tmp_df['Win'].shift(1)
            tmp_df = tmp_df[1:]
            appended_df_with_datediff = appended_df_with_datediff.append(tmp_df)
        
        if run_reg == True:
            print 'create regression '
            X = appended_df_with_datediff['Win_diff']
            Y = appended_df_with_datediff['Win']
            results = sm.OLS(Y,sm.add_constant(X)).fit()
            print results.summary()
            
            results = sm.Logit(Y,sm.add_constant(X)).fit()
            print results.summary()
            
        return appended_df_with_datediff[['GAME_ID','TEAM_ID','Win_diff']]
            
    def check_home_games_vs_away(self,run_reg):
        df = pd.read_csv('~//git/nba_analysis/files/game_info/appended_info_line_score.tsv',sep='\t')
        df_1 = pd.read_csv('~//git/nba_analysis/files/game_info/appended_info.tsv',sep='\t')
        df_1=df_1[['GAME_ID','HOME_TEAM_ID','VISITOR_TEAM_ID']]
        x = df_1.merge(df,how='inner',left_on=['GAME_ID','HOME_TEAM_ID'], right_on=['GAME_ID','TEAM_ID'])
        x['IsHome']=1
        x['IsAway']=0
        y = df_1.merge(df,how='inner',left_on=['GAME_ID','VISITOR_TEAM_ID'], right_on=['GAME_ID','TEAM_ID'])
        y['IsHome']=0
        y['IsAway']=1
        
        if run_reg == True:
            X = x[['PTS_QTR1','PTS_QTR2','PTS_QTR3','PTS_QTR4']]
            Y = x['Win']
            results = sm.OLS(Y,sm.add_constant(X)).fit()
            print results.summary()
        appended = x.append(y)
        return appended[['GAME_ID','TEAM_ID','IsHome','IsAway']]
    def get_avg_hom_away(self,row,a):
        tmp = a[a['GAME_DATE_EST_home']<row['GAME_DATE_EST_home']].groupby(['TEAM_ID_home','TEAM_ID_away']).mean()['Win_home'].reset_index()
        if len(tmp) == 0:
            return np.nan
        mean = tmp[(tmp['TEAM_ID_home'] == row['TEAM_ID_home'])&(tmp['TEAM_ID_away']==row['TEAM_ID_away']) ]['Win_home']
    #     print mean
        if len(mean) == 0:
            return np.nan
        else:
            return mean.values[0]
    def get_starter_dummy_per_game(self,bx):
        return bx[['GAME_ID']].merge(pd.get_dummies(bx['PLAYER_ID']),left_index=True,right_index=True).groupby('GAME_ID').sum()

    def test_combo_prev_win_and_day_rest(self,run_test,GET_FILE_FROM_LOCAL):    
        if GET_FILE_FROM_LOCAL ==True:
            team_predict = pd.read_csv('post_apply_team_vs_team_boxscore_csv.csv')
        else:
            df_rest = self.check_one_day_rest_vs_none(False)
            df_after_win = self.check_game_after_win(False)
            df_home_away = self.check_home_games_vs_away(False)
            
            
            
            
            merged = df_rest.merge(df_after_win,how='inner',left_on=['GAME_ID','TEAM_ID'],right_on=['GAME_ID','TEAM_ID'])
            merged=merged.merge(df_home_away,how='inner',left_on=['GAME_ID','TEAM_ID'],right_on=['GAME_ID','TEAM_ID'])
            merged['log_day_diff']=np.log(merged['day_diff'].astype(float))
            merged['Win'] = merged['Win'].astype(int)
            merged.drop_duplicates(inplace=True)
            a = merged.merge(merged,how='inner',left_on=['GAME_ID'] , right_on = ['GAME_ID'],suffixes = ['_home','_away'])
            a = a[a['TEAM_ID_home'] != a['TEAM_ID_away']]
            
            a = pd.concat([a, pd.get_dummies(a['TEAM_ID_home'], prefix='is_home_')], axis=1)
            a = pd.concat([a, pd.get_dummies(a['TEAM_ID_away'], prefix='is_away_')], axis=1)
            
            team_predict = a.copy()
            
           # team_predict = team_predict[:10]
            team_predict.to_csv('pre_apply_team_vs_team_boxscore_csv.csv')
            team_predict['nachi_chance'] = team_predict.apply(self.apply_team_vs_team_boxscore, args =(14,self.df,self.bx,),axis=1)
            team_predict.to_csv('post_apply_team_vs_team_boxscore_csv.csv')
        if run_test==True:
            team_predict = team_predict[team_predict['nachi_chance'].isnull()==False]
            team_predict = team_predict[team_predict['GAME_DATE_EST_home']>='2015-10-15']
            team_predict['nachi_chance_round'] = team_predict.nachi_chance.apply(np.round)
            
            player_dummy = self.get_starter_dummy_per_game(self.bx)
 
#             df = pd.concat([team_predict, pd.get_dummies(team_predict['TEAM_ID'], prefix='is_')], axis=1)
            X = team_predict.loc[:,team_predict.columns.str.startswith('is_') |
                                 team_predict.columns.str.startswith('PTS_Q')|
                                  team_predict.columns.isin( ['IsHome_home','day_diff_home','Win_diff_home', 'Win_diff_away' ,'day_diff_away' ,'nachi_chance_round'])]
            Y = team_predict['Win_home']
            X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.3, random_state=100)
            
            logreg = LogisticRegression()
            logreg.fit(X_train, y_train)
            y_pred = logreg.predict(X_test)
            print('Accuracy of logistic regression classifier on test set: {:.3f}'.format(logreg.score(X_test, y_test)))

            logreg = PassiveAggressiveClassifier()                                                                                   
            logreg.fit(X_train, y_train)                                                                                    
            y_pred = logreg.predict(X_test)                                                                                 
            print('Accuracy of logistic regression classifier on test set: {:.3f}'.format(logreg.score(X_test, y_test)))    
            
            logreg = Perceptron()                                                                                   
            logreg.fit(X_train, y_train)                                                                                    
            y_pred = logreg.predict(X_test)                                                                                 
            print('Accuracy of logistic regression classifier on test set: {:.3f}'.format(logreg.score(X_test, y_test)))
            
            logreg = LogisticRegressionCV()                                                                                   
            logreg.fit(X_train, y_train)                                                                                    
            y_pred = logreg.predict(X_test)                                                                                 
            print('Accuracy of logistic regression classifier on test set: {:.3f}'.format(logreg.score(X_test, y_test)))
            
            logreg = SGDClassifier()                                                                              
            logreg.fit(X_train, y_train)                                                                                 
            y_pred = logreg.predict(X_test)                                                                              
            print('Accuracy of logistic regression classifier on test set: {:.3f}'.format(logreg.score(X_test, y_test))) 
            

    def run_classifiers(self,X_train,y_train,X_test,y_test):
        
        print 'hi'
        
        clf = GaussianNB()
        clf.fit(X_train, y_train)
        clf.predict(X_test)
        print '************'
        print 'Naive Bayes: '
        print metrics.accuracy_score(y_test, clf.predict(X_test))
        print '************'
        
        from sklearn.svm import SVC
        clf = SVC(probability=True)
        clf.fit(X_train, y_train)
        clf.predict(X_test)
        print '************'
        print 'SVC : '
        print metrics.accuracy_score(y_test, clf.predict(X_test))
        print '************'
        
        
        from sklearn.ensemble import RandomForestClassifier as RF
        clf = RF()
        clf.fit(X_train, y_train)
        clf.predict(X_test)
        print '************'
        print 'Random Forrest: '
        print metrics.accuracy_score(y_test, clf.predict(X_test))
        print '************'
        
        from sklearn.neighbors import KNeighborsClassifier as KNN
        clf = KNN()
        clf.fit(X_train, y_train)
        clf.predict(X_test)
        print '************'
        print 'KNN: '
        print metrics.accuracy_score(y_test, clf.predict(X_test))
        print '************'
    
    def preprocess_df(self,df):
        from sklearn import preprocessing
        
        x = df.values #returns a numpy array
        column_names = df.columns
        min_max_scaler = preprocessing.MinMaxScaler()
        x_scaled = min_max_scaler.fit_transform(x)
        df_processed = pd.DataFrame(x_scaled,index=df.index)
        df_processed.columns = column_names
        return df_processed
    
    def get_playerlist_per_team(self ):
        
        run_pca = False
        
        print 'get_playerlist_per_team -------->'
        bx = pd.read_csv(RELATIVE_PATH + '/nba_analysis/files/boxscores/appended_boxscores.tsv',sep='\t')
        bx=bx.drop_duplicates()
        bx = self.convert_minutes_to_float(bx)
        del bx['Sec_Float']
        
        df = pd.read_csv(RELATIVE_PATH + '/nba_analysis/files/game_info/appended_info_line_score.tsv',sep='\t')
        df['GAME_DATE_EST'] = pd.to_datetime(df['GAME_DATE_EST'])
        df=df.drop_duplicates()
        x = df[['GAME_DATE_EST','GAME_ID','TEAM_ID']]
        bx_with_date = bx.merge(x,how='inner',left_on=['GAME_ID','TEAM_ID'],right_on=['GAME_ID','TEAM_ID'])
        bx_with_date['IsStarter'] = (bx_with_date['START_POSITION'].isnull()==False).astype(int)
        
        list_of_years = bx_with_date['GAME_DATE_EST'].dt.year.unique()
        appended_df = pd.DataFrame([])
        for year in list_of_years: 
            tmp_df = bx_with_date[bx_with_date['GAME_DATE_EST'].dt.year == year].copy()
            tmp_df = tmp_df[tmp_df['Total_Minutes_Float'].isnull()==False]
            mean = tmp_df.groupby('PLAYER_ID').mean()
            std = tmp_df.groupby('PLAYER_ID').std()
            sum_group = tmp_df.groupby('PLAYER_ID').sum()
            mean=mean.dropna()
            std=std.dropna()
            std.columns = [str(col) + '_std_'+str(year) for col in std.columns]
            mean.columns = [str(col) + '_mean_'+str(year) for col in mean.columns]
            sum_group.columns = [str(col) + '_sum_'+str(year) for col in sum_group.columns]
            std_mean_sum = std.merge(mean,how='inner',left_index=True,right_index=True).merge(sum_group,how='inner',left_index=True,right_index=True)
            appended_df =pd.concat([appended_df, std_mean_sum], axis=1)
        
        cols = [c for c in appended_df.columns if (c[:7] != 'GAME_ID')&(c[:7] != 'TEAM_ID')]
        appended_df=appended_df[cols]
        
        X_train = appended_df.copy()
        X_train=X_train.fillna(0)
        X_train = self.preprocess_df(X_train)
        number_of_clusters = 50
        max_combined_score = 0
        max_n_for_max_sill = 0
        max_dimensions_pca = 0
        for i in range(2,number_of_clusters):
            n=i
            model = KMeans(n_clusters=n, init='k-means++', max_iter=1000, n_init=1,random_state=5)
            res = model.fit(X_train)
            silhouette_avg = silhouette_score(X_train, model.labels_)
            
            if run_pca==True:
                from sklearn.decomposition import PCA
                for pca_num_dimensions in range(2,len(X_train.columns)-1):
                    pca = PCA(n_components=pca_num_dimensions)
                    X_r = pca.fit(X_train).transform(X_train)
                    pca.explained_variance_ratio_
                    df_X_r = pd.DataFrame(X_r)
                    model_pca = KMeans(n_clusters=n, init='k-means++', max_iter=1000, n_init=1,random_state=5)
                    res_pca = model_pca.fit(df_X_r)
                    
                    silhouette_avg_pca = silhouette_score(df_X_r, model_pca.labels_)
                
                    combined_score = (silhouette_avg_pca + silhouette_avg) * float(np.log(n))
    #             print("For n_clusters =", n,
    #                       "The average silhouette_score is :", silhouette_avg , " silhouette_avg_pca = " ,silhouette_avg_pca , 'total score of = ' , combined_score)
                
                    if max_combined_score<combined_score:
                        max_n_for_max_sill = n
                        max_combined_score = combined_score
                        max_dimensions_pca = pca_num_dimensions
            else:
                combined_score = ( silhouette_avg) * float(np.log(n))
                if max_combined_score<combined_score:
                        max_n_for_max_sill = n
                        max_combined_score = combined_score
                        
        print ("Decided to use " ,max_n_for_max_sill , " Clusters" )    
        model = KMeans(n_clusters=max_n_for_max_sill, init='k-means++', max_iter=1000, n_init=1,random_state=5) 
        res = model.fit(X_train)
               
        results = pd.DataFrame(data=model.labels_, columns=['cluster'], index=X_train.index)
        labeled = results.merge(X_train,how='inner',left_index=True,right_index=True)
        return labeled[['cluster']]
        
#         from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

    
    def predict_win(self,start_date,end_date):
        next_date_to_process = start_date
            
        df_predictions = pd.DataFrame(columns=['game_id','team_a','team_b','my_pred','actual_win'])
        total_checks = 0.0
        total_right_picks = 0.0
        df = pd.read_csv('~/git/nba_analysis/files/game_info/appended_info_line_score.tsv',sep='\t')
        df['GAME_DATE_EST'] = pd.to_datetime(df['GAME_DATE_EST'])
        df['Win_Loss_diff'] = df['TEAM_WINS_LOSSES'].str.split('-',expand=True)[0].astype(int)-df['TEAM_WINS_LOSSES'].str.split('-',expand=True)[1].astype(int)
        df=df.drop_duplicates()
        
        bx = pd.read_csv('~//git/nba_analysis/files/boxscores/appended_boxscores.tsv',sep='\t')
        bx=bx.drop_duplicates()
        bx = self.convert_minutes_to_float(bx)
        del bx['Sec_Float']
        
        while ( next_date_to_process < end_date ):
            game_ids = df[df['GAME_DATE_EST'] == next_date_to_process]['GAME_ID'].unique()
            for game_id in game_ids:
                teams = df[(df['GAME_DATE_EST'] == next_date_to_process)&(df['GAME_ID']==game_id)]['TEAM_ID'].values
                res = self.team_vs_team_boxscore(teams[0],teams[1],14,df,bx,next_date_to_process)
                team_a_name = df[(df['GAME_DATE_EST'] == next_date_to_process)&(df['GAME_ID']==game_id)&(df['TEAM_ID']==teams[0])]['TEAM_CITY_NAME'].values[0]
                team_b_name = df[(df['GAME_DATE_EST'] == next_date_to_process)&(df['GAME_ID']==game_id)&(df['TEAM_ID']==teams[1])]['TEAM_CITY_NAME'].values[0]
    
                relative_minutes_team_a = res[2]
                relative_minutes_team_b = res[3]
                
                is_team_a_win_predict = (res[0]*relative_minutes_team_a) >(res[1]*relative_minutes_team_b)
                actual_win_team_a = df[(df['GAME_DATE_EST'] == next_date_to_process)&(df['GAME_ID']==game_id)&(df['TEAM_ID']==teams[0])]['Win'].values[0]
                
                if actual_win_team_a== is_team_a_win_predict:
                    total_right_picks +=1.0
                total_checks +=1.0
                print ('Team A: ' , team_a_name ,' Score: ',res[0]*(relative_minutes_team_a/240), ' Team B = ' , team_b_name ,' Score: ',res[1]*(relative_minutes_team_b/240))
                df_predictions.loc[-1]=  [game_id,teams[0],teams[1],is_team_a_win_predict,actual_win_team_a]   
                df_predictions.index = df_predictions.index + 1                                        
                df_predictions = df_predictions.sort_index()                       
            next_date_to_process += relativedelta(days=1)
        
        x = self.test_combo_prev_win_and_day_rest(False)        
        
        tmp = x.rename(columns={'day_diff':'day_diff_home','Win_diff':'Win_diff_home','log_day_diff':'log_day_diff_home' })
        z= df_predictions.merge(tmp,how='inner',left_on=['game_id','team_a'],right_on=['GAME_ID','TEAM_ID'])      
        del z['GAME_ID']
        del z['TEAM_ID']
             
        tmp = x.rename(columns={'day_diff':'day_diff_away','Win_diff':'Win_diff_away','log_day_diff':'log_day_diff_away'})
        z= z.merge(tmp[['GAME_ID','TEAM_ID','day_diff_away','Win_diff_away','log_day_diff_away']],how='inner',left_on=['game_id','team_b'],right_on=['GAME_ID','TEAM_ID'])           
        del z['GAME_ID']
        del z['TEAM_ID']
        
        z['my_pred']=z['my_pred'].astype(int)
        z['actual_win'] = z['actual_win'].astype(int)
        
        X = z[['my_pred','Win_diff_home','IsHome','log_day_diff_home', 'Win_diff_away','log_day_diff_away']]
        Y = z[['actual_win']]
        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, random_state=1)
        
        # run classifiers
        results_logit = sm.Logit(y_train,sm.add_constant(X_train)).fit()
        results_probit=  sm.Probit(y_train,sm.add_constant(X_train)).fit()
        results_ols =   sm.OLS(y_train,sm.add_constant(X_train)).fit()
        self.run_classifiers(X_train,y_train,X_test,y_test)
        
        print results_ols.summary()
        print ("I was right "  , float(total_right_picks/total_checks) , ' % of the time')   
        df_predictions['success'] = df_predictions['my_pred']==df_predictions['actual_win'] 
        print df_predictions['success'] .mean() 
        return df_predictions 
         
    
    
    def cnn_on_df(self,X,y):
        from sklearn.model_selection import train_test_split
        X_train, X_test, y_train, y_test = train_test_split(X, y)
    
        from sklearn.preprocessing import StandardScaler
        scaler = StandardScaler()
        scaler.fit(X_train)
        X_train = scaler.transform(X_train)
        X_test = scaler.transform(X_test)
    
    
        from sklearn.neural_network import MLPClassifier
        mlp = MLPClassifier(hidden_layer_sizes=(13,13,13),max_iter=500)
        mlp.fit(X_train,y_train)
    
    
        predictions = mlp.predict(X_test)
        from sklearn.metrics import classification_report,confusion_matrix
        print(confusion_matrix(y_test,predictions))
        print(classification_report(y_test,predictions))
        
          
    def team_vs_team_boxscore(self,team_a,team_b,days_back_roster,df,bx,current_date):
        print 'team_vs_team_boxscore ------>'
        
#         df = pd.read_csv('~//git/nba_analysis/files/game_info/appended_info_line_score.tsv',sep='\t')
        df['GAME_DATE_EST'] = pd.to_datetime(df['GAME_DATE_EST'])
        df=df.drop_duplicates()
        teams = df['TEAM_ID'].unique()
        
        days_back_filter = current_date- datetime.timedelta(days=days_back_roster)
        team_a_game_list_two_weeks = df[(df['TEAM_ID']==team_a)&(df['GAME_DATE_EST']>=days_back_filter)]
        team_b_game_list_two_weeks = df[(df['TEAM_ID']==team_b)&(df['GAME_DATE_EST']>=days_back_filter)]
        
        list_of_games_a = team_a_game_list_two_weeks['GAME_ID'].unique()
        list_of_games_b = team_b_game_list_two_weeks['GAME_ID'].unique()
        
        
#         bx = pd.read_csv('~//git/nba_analysis/files/boxscores/appended_boxscores.tsv',sep='\t')
#         bx=bx.drop_duplicates()
#         bx = self.convert_minutes_to_float(bx)
#         del bx['Sec_Float']
#         
        #get player list
        bx_a_players = bx[(bx['TEAM_ID']==team_a)&(bx['GAME_ID'].isin(list_of_games_a))]['PLAYER_ID'].unique()
        bx_b_players = bx[(bx['TEAM_ID']==team_b)&(bx['GAME_ID'].isin(list_of_games_b))]['PLAYER_ID'].unique()
        
        mean_minutes_team_a = bx[(bx['TEAM_ID']==team_a)&(bx['GAME_ID'].isin(list_of_games_a))].groupby('PLAYER_ID').mean()['Total_Minutes_Float']
        mean_minutes_team_b = bx[(bx['TEAM_ID']==team_b)&(bx['GAME_ID'].isin(list_of_games_b))].groupby('PLAYER_ID').mean()['Total_Minutes_Float']
        mean_minutes_team_a = mean_minutes_team_a*240/mean_minutes_team_a.sum()
        mean_minutes_team_b = mean_minutes_team_b*240/mean_minutes_team_b.sum()
        #############
        #
        # get all players that are similar to each of the players
        # use kmeans or KNN
        # 
#         self.labled = self.get_playerlist_per_team()
        
        list_of_all_games_against_b = df[(df['TEAM_ID']==team_b)]['GAME_ID'].values
        team_a_accumulated_pts_per_minute = 0.0
        total_number_of_minutes_per_team_a = 0.0
        for curr_player in bx_a_players:
            if (curr_player in self.labled.index) and (curr_player in mean_minutes_team_a.index):
                cluster_num = self.labled.ix[curr_player]
                similar_players = self.labled[self.labled['cluster']==int(cluster_num)].index
                filter_box_score_to_similar_players = bx[bx['PLAYER_ID'].isin(similar_players)]
                filter_against_team = filter_box_score_to_similar_players[filter_box_score_to_similar_players['GAME_ID'].isin(list_of_all_games_against_b)]
                pts_per_min_avg_of_cluster = filter_against_team.mean()['PTS']/filter_against_team.mean()['Total_Minutes_Float']
                if math.isnan(mean_minutes_team_a.ix[curr_player]):
                    continue
                pts_per_min_player = mean_minutes_team_a.ix[curr_player] * pts_per_min_avg_of_cluster
                total_number_of_minutes_per_team_a += mean_minutes_team_a.ix[curr_player]
                team_a_accumulated_pts_per_minute = team_a_accumulated_pts_per_minute + pts_per_min_player

        list_of_all_games_against_a = df[(df['TEAM_ID']==team_a)]['GAME_ID'].values
        team_b_accumulated_pts_per_minute = 0.0
        total_number_of_minutes_per_team_b = 0.0
        for curr_player in bx_b_players:
            if (curr_player in self.labled.index) and (curr_player in mean_minutes_team_b.index):
                cluster_num = self.labled.ix[curr_player]
                similar_players = self.labled[self.labled['cluster']==int(cluster_num)].index
                filter_box_score_to_similar_players = bx[bx['PLAYER_ID'].isin(similar_players)]
                filter_against_team = filter_box_score_to_similar_players[filter_box_score_to_similar_players['GAME_ID'].isin(list_of_all_games_against_a)]
                pts_per_min_avg_of_cluster = filter_against_team.mean()['PTS']/filter_against_team.mean()['Total_Minutes_Float']
                if math.isnan(mean_minutes_team_b.ix[curr_player]):
                    continue
                pts_per_min_player = mean_minutes_team_b.ix[curr_player] * pts_per_min_avg_of_cluster
                team_b_accumulated_pts_per_minute = team_b_accumulated_pts_per_minute + pts_per_min_player
                total_number_of_minutes_per_team_b += mean_minutes_team_b.ix[curr_player]
#         print 'team_vs_team_boxscore <------'
        return [ team_a_accumulated_pts_per_minute,team_b_accumulated_pts_per_minute,total_number_of_minutes_per_team_a,total_number_of_minutes_per_team_b]
        # filter what those players did vs team B - calc AVG
        #############
    
    
    
    
    
    
    def apply_team_vs_team_boxscore(self,row,days_back_roster,df,bx):
        print 'team_vs_team_boxscore ------>'
        print row['GAME_DATE_EST_home']
        current_date = row['GAME_DATE_EST_home']
        team_a =  row['TEAM_ID_home']
        team_b =  row['TEAM_ID_away']
        
#         team_a,team_b,days_back_roster,df,bx,current_date
#         df = pd.read_csv('~//git/nba_analysis/files/game_info/appended_info_line_score.tsv',sep='\t')
        df['GAME_DATE_EST'] = pd.to_datetime(df['GAME_DATE_EST'])
        df=df.drop_duplicates()
        teams = df['TEAM_ID'].unique()
        
        days_back_filter = current_date- datetime.timedelta(days=days_back_roster)
        team_a_game_list_two_weeks = df[(df['TEAM_ID']==team_a)&(df['GAME_DATE_EST']>=days_back_filter)]
        team_b_game_list_two_weeks = df[(df['TEAM_ID']==team_b)&(df['GAME_DATE_EST']>=days_back_filter)]
        
        list_of_games_a = team_a_game_list_two_weeks['GAME_ID'].unique()
        list_of_games_b = team_b_game_list_two_weeks['GAME_ID'].unique()
        
#         bx = pd.read_csv('~//git/nba_analysis/files/boxscores/appended_boxscores.tsv',sep='\t')
#         bx=bx.drop_duplicates()
#         bx = self.convert_minutes_to_float(bx)
#         del bx['Sec_Float']
#         
        #get player list
        bx_a_players = bx[(bx['TEAM_ID']==team_a)&(bx['GAME_ID'].isin(list_of_games_a))]['PLAYER_ID'].unique()
        bx_b_players = bx[(bx['TEAM_ID']==team_b)&(bx['GAME_ID'].isin(list_of_games_b))]['PLAYER_ID'].unique()
        
        mean_minutes_team_a = bx[(bx['TEAM_ID']==team_a)&(bx['GAME_ID'].isin(list_of_games_a))].groupby('PLAYER_ID').mean()['Total_Minutes_Float']
        mean_minutes_team_b = bx[(bx['TEAM_ID']==team_b)&(bx['GAME_ID'].isin(list_of_games_b))].groupby('PLAYER_ID').mean()['Total_Minutes_Float']
        mean_minutes_team_a = mean_minutes_team_a*240/mean_minutes_team_a.sum()
        mean_minutes_team_b = mean_minutes_team_b*240/mean_minutes_team_b.sum()
        #############
        #
        # get all players that are similar to each of the players
        # use kmeans or KNN
        # 
#         self.labled = self.get_playerlist_per_team()
        
        list_of_all_games_against_b = df[(df['TEAM_ID']==team_b)]['GAME_ID'].values
        team_a_accumulated_pts_per_minute = 0.0
        total_number_of_minutes_per_team_a = 0.0
        for curr_player in bx_a_players:
            if (curr_player in self.labled.index) and (curr_player in mean_minutes_team_a.index):
                cluster_num = self.labled.ix[curr_player]
                similar_players = self.labled[self.labled['cluster']==int(cluster_num)].index
                filter_box_score_to_similar_players = bx[bx['PLAYER_ID'].isin(similar_players)]
                filter_against_team = filter_box_score_to_similar_players[filter_box_score_to_similar_players['GAME_ID'].isin(list_of_all_games_against_b)]
                pts_per_min_avg_of_cluster = filter_against_team.mean()['PTS']/filter_against_team.mean()['Total_Minutes_Float']
                if math.isnan(mean_minutes_team_a.ix[curr_player]):
                    continue
                pts_per_min_player = mean_minutes_team_a.ix[curr_player] * pts_per_min_avg_of_cluster
                total_number_of_minutes_per_team_a += mean_minutes_team_a.ix[curr_player]
                team_a_accumulated_pts_per_minute = team_a_accumulated_pts_per_minute + pts_per_min_player

        list_of_all_games_against_a = df[(df['TEAM_ID']==team_a)]['GAME_ID'].values
        team_b_accumulated_pts_per_minute = 0.0
        total_number_of_minutes_per_team_b = 0.0
        for curr_player in bx_b_players:
            if (curr_player in self.labled.index) and (curr_player in mean_minutes_team_b.index):
                cluster_num = self.labled.ix[curr_player]
                similar_players = self.labled[self.labled['cluster']==int(cluster_num)].index
                filter_box_score_to_similar_players = bx[bx['PLAYER_ID'].isin(similar_players)]
                filter_against_team = filter_box_score_to_similar_players[filter_box_score_to_similar_players['GAME_ID'].isin(list_of_all_games_against_a)]
                pts_per_min_avg_of_cluster = filter_against_team.mean()['PTS']/filter_against_team.mean()['Total_Minutes_Float']
                if math.isnan(mean_minutes_team_b.ix[curr_player]):
                    continue
                pts_per_min_player = mean_minutes_team_b.ix[curr_player] * pts_per_min_avg_of_cluster
                team_b_accumulated_pts_per_minute = team_b_accumulated_pts_per_minute + pts_per_min_player
                total_number_of_minutes_per_team_b += mean_minutes_team_b.ix[curr_player]
        print 'team_vs_team_boxscore <------'
        return float(team_a_accumulated_pts_per_minute) / (float(team_b_accumulated_pts_per_minute) + float(team_a_accumulated_pts_per_minute))
        # filter what those players did vs team B - calc AVG
        #############
    
    
    
    def predict_win_single_team(self,team_a,team_b,start_date):
        next_date_to_process = start_date
            
        df_predictions = pd.DataFrame(columns=['team_a','team_b','my_pred','actual_win'])
        total_checks = 0.0
        total_right_picks = 0.0
        df = pd.read_csv('~/git/nba_analysis/files/game_info/appended_info_line_score.tsv',sep='\t')
        df['GAME_DATE_EST'] = pd.to_datetime(df['GAME_DATE_EST'])
        df=df.drop_duplicates()
        
        bx = pd.read_csv('~/git/nba_analysis/files/boxscores/appended_boxscores.tsv',sep='\t')
        bx=bx.drop_duplicates()
        bx = self.convert_minutes_to_float(bx)
        del bx['Sec_Float']
        res = self.team_vs_team_boxscore(team_a,team_b,14,df,bx,next_date_to_process)
        df_rest = self.check_one_day_rest_vs_none(False)
        df_after_win = self.check_game_after_win(False)
        df_home_away = self.check_home_games_vs_away(False)
    
    def predict_spread(self,run_test,GET_FILE_FROM_LOCAL):
        if GET_FILE_FROM_LOCAL ==True:
            team_predict = pd.read_csv('post_apply_team_vs_team_boxscore_csv.csv')
        else:
            df_rest = self.check_one_day_rest_vs_none(False)
            df_after_win = self.check_game_after_win(False)
            df_home_away = self.check_home_games_vs_away(False)
            
            df = pd.read_csv('~/git/nba_analysis/files/game_info/appended_info_line_score.tsv',sep='\t')
            x = df.merge(df,how='inner',left_on=['GAME_ID'],right_on=['GAME_ID'],suffixes=['_current','_opposing'])
            x = x[(x['TEAM_ID_current']!=x['TEAM_ID_opposing'])]
            x['spread']=x['PTS_current']- x['PTS_opposing']
            x.drop_duplicates(inplace=True)
            spread_df = x[['GAME_DATE_EST_current','GAME_ID','TEAM_ID_current','TEAM_ID_opposing','spread']]
            spread_df['GAME_DATE_EST_current']=pd.to_datetime(spread_df['GAME_DATE_EST_current'])
            past_x_games = [1,2,3,4,5]
            for past_x_game in past_x_games:
                spread_df['past_'+str(past_x_game)] = spread_df.apply(self.get_spread_past_x_game, args =(spread_df,past_x_game,'spread',),axis=1)
            
            
            sm
            # quick test on spread vs past spreads
            q = spread_df.dropna()
            X=q[['past_1','past_2','past_3','past_4','past_5']]
            Y = q[['spread']]
            print sm.OLS(Y,X).fit().summary()
            
            
            
            
            merged = df_rest.merge(df_after_win,how='inner',left_on=['GAME_ID','TEAM_ID'],right_on=['GAME_ID','TEAM_ID'])
            merged=merged.merge(df_home_away,how='inner',left_on=['GAME_ID','TEAM_ID'],right_on=['GAME_ID','TEAM_ID'])
            merged['log_day_diff']=np.log(merged['day_diff'].astype(float))
            merged['Win'] = merged['Win'].astype(int)
            merged.drop_duplicates(inplace=True)
            a = merged.merge(merged,how='inner',left_on=['GAME_ID'] , right_on = ['GAME_ID'],suffixes = ['_home','_away'])
            a = a[a['TEAM_ID_home'] != a['TEAM_ID_away']]
            
            a = pd.concat([a, pd.get_dummies(a['TEAM_ID_home'], prefix='is_home_')], axis=1)
            a = pd.concat([a, pd.get_dummies(a['TEAM_ID_away'], prefix='is_away_')], axis=1)
            
            team_predict = a.copy()
            
           # team_predict = team_predict[:10]
            team_predict.to_csv('pre_apply_team_vs_team_boxscore_csv.csv')
            team_predict['nachi_chance'] = team_predict.apply(self.apply_team_vs_team_boxscore, args =(14,self.df,self.bx,),axis=1)
            team_predict.to_csv('post_apply_team_vs_team_boxscore_csv.csv')
        if run_test==True:
            team_predict = team_predict[team_predict['nachi_chance'].isnull()==False]
            team_predict = team_predict[team_predict['GAME_DATE_EST_home']>='2015-10-15']
            team_predict['nachi_chance_round'] = team_predict.nachi_chance.apply(np.round)
            
            player_dummy = self.get_starter_dummy_per_game(self.bx)
 
#             df = pd.concat([team_predict, pd.get_dummies(team_predict['TEAM_ID'], prefix='is_')], axis=1)
            X = team_predict.loc[:,team_predict.columns.str.startswith('is_') |
                                 team_predict.columns.str.startswith('PTS_Q')|
                                  team_predict.columns.isin( ['IsHome_home','day_diff_home','Win_diff_home', 'Win_diff_away' ,'day_diff_away' ,'nachi_chance_round'])]
            Y = team_predict['Win_home']
            X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.3, random_state=100)
            
            logreg = LogisticRegression()
            logreg.fit(X_train, y_train)
            y_pred = logreg.predict(X_test)
            print('Accuracy of logistic regression classifier on test set: {:.3f}'.format(logreg.score(X_test, y_test)))

            logreg = PassiveAggressiveClassifier()                                                                                   
            logreg.fit(X_train, y_train)                                                                                    
            y_pred = logreg.predict(X_test)                                                                                 
            print('Accuracy of logistic regression classifier on test set: {:.3f}'.format(logreg.score(X_test, y_test)))    
            
            logreg = Perceptron()                                                                                   
            logreg.fit(X_train, y_train)                                                                                    
            y_pred = logreg.predict(X_test)                                                                                 
            print('Accuracy of logistic regression classifier on test set: {:.3f}'.format(logreg.score(X_test, y_test)))
            
            logreg = LogisticRegressionCV()                                                                                   
            logreg.fit(X_train, y_train)                                                                                    
            y_pred = logreg.predict(X_test)                                                                                 
            print('Accuracy of logistic regression classifier on test set: {:.3f}'.format(logreg.score(X_test, y_test)))
            
            logreg = SGDClassifier()                                                                              
            logreg.fit(X_train, y_train)                                                                                 
            y_pred = logreg.predict(X_test)                                                                              
            print('Accuracy of logistic regression classifier on test set: {:.3f}'.format(logreg.score(X_test, y_test))) 
            
    
    def get_spread_past_x_game(self,row,a,past_x_game,column):
        tmp = a[(a['GAME_DATE_EST_current']<row['GAME_DATE_EST_current'])&(a['TEAM_ID_current']==row['TEAM_ID_current'])]
        try:
            return tmp.sort_values('GAME_DATE_EST_current').iloc[past_x_game][column]    
        except Exception as e:
            return np.nan
        
    def quick_check_close_games(self):
        print 'quick_check_close_games---->'
        CREATE_DF =True
        if CREATE_DF == True:
            cut_df = self.df[['GAME_ID','TEAM_ID','PTS','Win']]
            m = cut_df.merge(cut_df,how='left',left_on=['GAME_ID'],right_on=['GAME_ID'],suffixes=['_team_a','_team_b'])
            m = m[m['TEAM_ID_team_a'] != m['TEAM_ID_team_b']]
            cut_bx = self.bx[self.bx['Total_Minutes_Float']>2][['TEAM_ID','PLAYER_ID','PLAYER_NAME','Total_Minutes_Float','GAME_ID']]
            merged_bx = cut_bx.merge(m,how='inner',left_on=['TEAM_ID','GAME_ID'],right_on=['TEAM_ID_team_a','GAME_ID'])
            merged_bx.drop_duplicates(inplace=True)
            merged_bx.to_csv('~/merged_bx.tsv',sep='\t')
            print 'done'
        merged_bx['pts_diff'] = merged_bx['PTS_team_a'] - merged_bx['PTS_team_b']
        merged_bx = merged_bx[abs(merged_bx['pts_diff'])<=3]
        merged_bx = merged_bx.groupby(by='PLAYER_NAME').filter(lambda x: len(x) > 40)
        mean = merged_bx.groupby('PLAYER_NAME').mean()[['Total_Minutes_Float','Win_team_a']]
        count = merged_bx.groupby('PLAYER_NAME').count()[['Win_team_a']]
        std = merged_bx.groupby('PLAYER_NAME').std()[['Win_team_a']]
        mean_count = mean.merge(count,how='inner',left_index=True,right_index=True).merge(std,how='inner',left_index=True,right_index=True) 
        mean_count.rename(columns={'Win_team_a_x':'Win_Percenatge' ,
                                     'Win_team_a_y':'Number_Of_Observations',
                                     'Win_team_a':'STD' },inplace=True)
        mean_count = mean_count.sort_values('Win_Percenatge',ascending=False) 
        mean_count['t'] = mean_count['Win_Percenatge']/mean_count['STD']*np.sqrt(mean_count['Number_Of_Observations'])
        mean_count[0:6].append(mean_count[-10:])['Win_Percenatge'].plot(kind='bar',rot=90)
        plt.suptitle('Win Percentages In 1 Possession Games')
        plt.xlabel('Player Name')
        plt.ylabel('Win Percentages')
        plt.show()
        print 'quick_check_close_games <----'
        merged_bx
        
        m = self.df.merge(self.df,how='left',left_on=['GAME_ID'],right_on=['GAME_ID'],suffixes=['_team_a','_team_b'])
        cut_bx = self.bx[self.bx['Total_Minutes_Float']>2]
        merged_bx = cut_bx.merge(m,how='inner',left_on=['TEAM_ID','GAME_ID'],right_on=['TEAM_ID_team_a','GAME_ID'])
        merged_bx.drop_duplicates(inplace=True)
        z = merged_bx.groupby(['TEAM_ID','GAME_ID']).std()[['Total_Minutes_Float']].reset_index()
        
        x = z[['Total_Minutes_Float']]
        y = z[['Win_team_a']]
        X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=100)
        logreg = LogisticRegression()
        logreg.fit(X_train, y_train)
        y_pred = logreg.predict(X_test)
        print('Accuracy of logistic regression classifier on test set: {:.3f}'.format(logreg.score(X_test, y_test)))
        
if __name__ == '__main__':
    import sys
    try:
        function_name       = sys.argv[1]
    except:
        function_name       = 'quick_check_close_games'
    data_test = TestsNBA()
    
    

    if function_name == 'check_trend_plus_minus':
        data_test.check_trend_plus_minus()
    if function_name == 'check_home_games_vs_away':
        data_test.check_home_games_vs_away(False)
    if function_name == 'check_one_day_rest_vs_none':
        data_test.check_one_day_rest_vs_none(True)
    if function_name == 'check_game_after_win':
        data_test.check_game_after_win(True)
    if function_name == 'test_combo_prev_win_and_day_rest':
        data_test.test_combo_prev_win_and_day_rest(True,True)
    if function_name == 'team_vs_team_boxscore':
        data_test.team_vs_team_boxscore(1610612752,1610612737,14)
    if function_name == 'predict_win':
        data_test.predict_win(parser.parse('2017-11-15'),parser.parse('2017-11-22'))
    if function_name == 'predict_win_single_team':
        data_test.predict_win_single_team(1610612752,1610612737,parser.parse('2017-11-22'))
    if function_name == 'predict_spread':
        data_test.predict_spread(True,False)
    if function_name == 'quick_check_close_games':
        data_test.quick_check_close_games()
        
# https://programtalk.com/vs2/?source=python/11746/nba_py/examples/example_team.py#
