import os
import pandas as pd
import matplotlib.pyplot as plt 
from pandas.stats.api import ols
import statsmodels.api as sm
import numpy as np
import datetime as dt
import six
from PIL import Image

HOME_PATH = '~/git/nba_analysis/files/'

class DailyPlayers(object):
    def __init__(self):
        print 'init...'
        self.df_logs  = pd.read_csv('~/git/personal/files/personal_game_logs/appended_info.tsv',sep='\t')
        self.df_logs  = self.df_logs.drop_duplicates()
        
        self.df_logs['GAME_DATE'] = pd.to_datetime(self.df_logs['GAME_DATE'],coerce=True)
        self.update_player_list()
        
    # Input of this function is a data frame. The output is a fig of a nice styled table.
    def render_mpl_table(self,data, col_width=3.0, row_height=0.625, font_size=14,
                         header_color='#40466e', row_colors=['#f1f1f2', 'w'], edge_color='w',
                         bbox=[0, 0, 1, 1], header_columns=0,
                         ax=None, **kwargs):
        
    
        if ax is None:
            size = (np.array(data.shape[::-1]) + np.array([0, 1])) * np.array([col_width, row_height])
            fig, ax = plt.subplots(figsize=size)
            ax.axis('off')
    
        mpl_table = ax.table(cellText=data.values, bbox=bbox, colLabels=data.columns, **kwargs)
    
        mpl_table.auto_set_font_size(False)
        mpl_table.set_fontsize(font_size)
    
        for k, cell in  six.iteritems(mpl_table._cells):
            cell.set_edgecolor(edge_color)
            if k[0] == 0 or k[1] < header_columns:
                cell.set_text_props(weight='bold', color='w')
                cell.set_facecolor(header_color)
            else:
                cell.set_facecolor(row_colors[k[0]%len(row_colors) ])
        return mpl_table

    def plot_dataframe(self,df,file_path,file_name,player_code) :
        plt.close()
        x = self.render_mpl_table(df, header_columns=0, col_width=3.5,font_size=20)
        plt.suptitle('Name : ' + player_code)
        plt.savefig(file_path + file_name)
#         plt.show()
    
    
    def regression_column(self,df_logs,player_id,column_name):
        print 'regression_column ---------->'
        df = df_logs[df_logs['Player_ID']==player_id]
        x = df.set_index('GAME_DATE')
        
        # create 5,15,25 rolling mean
        x.sort_index(inplace=True)
        a = pd.rolling_mean(x[[column_name]],window=5,min_periods=1).shift(-5)
        a.columns = [str(col) + '_5' for col in a.columns]
        b =  pd.rolling_mean(x[[column_name]],window=15,min_periods=1).shift(-20)
        b.columns = [str(col) + '_15' for col in b.columns]
        c =  pd.rolling_mean(x[[column_name]],window=25,min_periods=1).shift(-45)
        c.columns = [str(col) + '_25' for col in c.columns]
    
        z = x[[column_name]].merge(a,how='inner',left_index=True,right_index=True).merge(b,how='inner',left_index=True,right_index=True).merge(c,how='inner',left_index=True,right_index=True)
        print 'regression_column <----------'
        return z
    
    def perform_regression(self,column,q):
        q.dropna(inplace=True)
        Y = q[column]
        X=q[[column + '_5',column + '_15',column +'_25']]
        results = sm.OLS(Y,sm.add_constant(X)).fit()
        
        print results.summary()
    
    def plot_time_series_per_column(self,column,q,axes,plot_index):
        q.dropna(inplace=True)
        X = (q.index - dt.datetime(2014,1,1)).astype('timedelta64[D]')
        Y = q[column]
    
        
        results = sm.OLS(Y,sm.add_constant(X)).fit()
        tstat = results.tvalues
        
        rolled = pd.rolling_mean(q, 10)[column].dropna()
#         rolled.reset_index().plot()
        
        axes.flatten()[plot_index].scatter(q.reset_index().index,Y)
        axes.flatten()[plot_index].plot(rolled.reset_index()[column],color='red')
#         plot regression
#         axes.flatten()[plot_index].plot(q.reset_index().index, X*results.params[1] + results.params[0])

        axes.flatten()[plot_index].set_title(column + ' Regression Analysis ,\n Significance Score = ' +str(round(tstat[1],3)) +\
                                             '\n Time Related Variation Explained  = ' + str(round(results.rsquared,3)*100) + '%')
    #     axes.flatten()[plot_index].legend(str(tstat))
    #     plt.show() 
    
    def check_attribute(self,column_list,player_id,player_code):
        file_path = '~//git/personal/files/daily_player/'
        fig, axes = plt.subplots(3,2, figsize=(15, 6), facecolor='w', edgecolor='k')
        fig.autofmt_xdate(rotation=90)
        fig.subplots_adjust(hspace=1)
        plot_index = 0 
        
        for column in column_list:
            df = self.regression_column(self.df_logs,player_id,column)
    #         perform_regression(column,df)
            if len(df) > 0:
                self.plot_time_series_per_column(column,df,axes,plot_index)
            plot_index = plot_index + 1
        fig.savefig(file_path + player_code)
        plt.close(fig)
        description_df = self.df_logs[self.df_logs['Player_ID']==player_id].describe()
        description_df=description_df[['MIN', 'FG_PCT','FGM', 'FGA',
                                        'FT_PCT', 'OREB', 'DREB', 'REB', 'AST', 'STL',
                                       'BLK', 'TOV', 'PTS', 'PLUS_MINUS']].round(2).reset_index()
        description_df.rename(columns={'PLUS_MINUS':'+-' },inplace=True)  
        description_df=description_df.ix[1:]                             
        self.plot_dataframe(description_df,file_path,player_code + '_desc',player_code)
        res = self.append_pics_top_bottom(file_path,player_code+ '_desc.png',player_code+'.png' , file_path+'appended_results/'+player_code+'.png')
#         res = self.append_jpegs_side_by_side(file_path,player_code+'.png',player_code+ '_desc.png' , file_path+'appended_results/'+player_code+'.png')
        print res
    #     plt.show()
    
    def append_pics_top_bottom(self,file_path,path_1,path_2,result_path):
        path= file_path
        list_im = [path+path_1,path+path_2]
        imgs    = [ Image.open(i) for i in list_im ]
        
        min_shape = sorted( [(np.sum(i.size), i.size ) for i in imgs])[0][1]
        imgs_comb = np.hstack( (np.asarray( i.resize(min_shape) ) for i in imgs ) )
        
        imgs_comb = np.vstack( (np.asarray( i.resize(min_shape) ) for i in imgs ) )
        imgs_comb = Image.fromarray( imgs_comb)
        imgs_comb.save( result_path )
        return True
    
    def append_jpegs_side_by_side(self,file_path,path_1,path_2,result_path):
        images = map(Image.open, [file_path+path_1, file_path+path_2])
        widths, heights = zip(*(i.size for i in images))
        
        total_width = sum(widths)
        max_height = max(heights)
        
        new_im = Image.new('RGB', (total_width, max_height))
        
        x_offset = 0
        for im in images:
            new_im.paste(im, (x_offset,0))
            x_offset += im.size[0]
        
        new_im.save(result_path) 
        return True   
    
    
    def get_daily_player_on_all(self):  
        df_players= pd.read_csv(HOME_PATH + 'boxscores/appended_boxscores.tsv' ,sep='\t')
        list_players = df_players[['PLAYER_ID','PLAYER_NAME']].values
        for row in list_players:
            print row
            try:
                self.check_attribute(['AST','PTS','STL','BLK','PLUS_MINUS','TOV'],row[0],row[1])
                
            except:
                continue
    
    
    def update_player_list(self):
        print 'update_player_list -------->'
        df = pd.read_csv('~/git/personal/files/boxscores/appended_boxscores.tsv',sep='\t')
        df[['PLAYER_ID','PLAYER_NAME']].drop_duplicates().to_csv( HOME_PATH + 'boxscores/player_list.tsv',sep='\t',index=False)
        print 'update_player_list <--------'
        
if __name__ == '__main__':
    import sys
    daily_player_obj = DailyPlayers()
    daily_player_obj.get_daily_player_on_all()    

    
    
# https://programtalk.com/vs2/?source=python/11746/nba_py/examples/example_team.py#
