import pandas as pd
import numpy as np
import os

import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.model_selection import KFold, cross_val_score
from sklearn.grid_search import GridSearchCV



df = pd.read_csv('input/shot_logs.csv')
df['SHOT_DIST_ROUND'] = df['SHOT_DIST'].round()
df['SHOT_CLOCK_ROUND'] = df['SHOT_CLOCK'].round()
df['CLOSE_DEF_DIST_ROUND'] = df['CLOSE_DEF_DIST'].round()
df['BALL_HOG'] = None
df['SHOT_NUMBER_CAT']=None
df.loc[df['TOUCH_TIME']<=1,'BALL_HOG']='<=1'
df.loc[df['TOUCH_TIME']>15,'BALL_HOG']='>15'
df.loc[(df['TOUCH_TIME']<=15)&(df['TOUCH_TIME']>1),'BALL_HOG']='between 1 and 15'
df.loc[df['SHOT_NUMBER']==1,'SHOT_NUMBER_CAT']='=1'
df.loc[df['SHOT_NUMBER']>1,'SHOT_NUMBER_CAT']='>1'
df['FirstShotQ'] = (df['SHOT_NUMBER']==1).astype(int) * df['PERIOD']

df['SHOT_RESULT_BINARY'] = (df['SHOT_RESULT']=='made').astype(int)

x = df[['SHOT_CLOCK_ROUND',
        'CLOSE_DEF_DIST_ROUND',
        'BALL_HOG',
        'DRIBBLES',
        'SHOT_DIST_ROUND',
        'SHOT_NUMBER',
        'PTS_TYPE',
        'player_name',
        'CLOSEST_DEFENDER',
        'SHOT_NUMBER_CAT',
        'FirstShotQ'
        ]].fillna(0)
dummy_features_BALL_HOG = pd.get_dummies(x['BALL_HOG'])
x = pd.concat([x, pd.get_dummies(x['BALL_HOG'], prefix='hog_')], axis=1)
del x['BALL_HOG']
x = pd.concat([x, pd.get_dummies(x['player_name'], prefix='player_')], axis=1)
del x['player_name']
x = pd.concat([x, pd.get_dummies(x['CLOSEST_DEFENDER'], prefix='def_')], axis=1)
del x['CLOSEST_DEFENDER']
x = pd.concat([x, pd.get_dummies(x['SHOT_NUMBER_CAT'],prefix = 'shot_cat_')], axis=1)
del x['SHOT_NUMBER_CAT']

y = df['SHOT_RESULT_BINARY']
from sklearn.model_selection import train_test_split
X_train,X_test,y_train,y_test = train_test_split(x,y, test_size=0.2)




seed = 7
models = []
#models.append(('LR', LogisticRegression()))
# models.append(('LDA', LinearDiscriminantAnalysis()))
# models.append(('KNN', KNeighborsClassifier()))
knn_params = {'n_neighbors':[3,5,7,9,11],
              'weights':['uniform','distance'],
              'metric':['euclidean','manhattan']}

#models.append(('CART', DecisionTreeClassifier()))
tree_para = {
             # 'splitter': ['best', 'random'],
             #'learning_rate':[0.01, 0.025, 0.05, 0.075, 0.1, 0.15, 0.2],
             'max_depth':[7,8,10],
             'min_samples_split' : [10,20,50,100,150,200]
             # 'class_weight' : ['balanced',None]
             }
             
             
models.append(('GRID', GridSearchCV(DecisionTreeClassifier(), tree_para, verbose = 10))) #0.615746
# models.append(('GRID', GridSearchCV(KNeighborsClassifier(), knn_params , cv=3,n_jobs=1))) #
models[0][1].fit(x,y)
print(models[0][1].grid_scores_)
print(models[0][1].best_score_)
print(models[0][1].best_params_)

#models.append(('NB', GaussianNB()))
# models.append(('SVM', SVC()))
results = []
names = []
scoring = 'accuracy'
for name, model in models:
    print(name)
    kfold = model_selection.KFold(n_splits=5, random_state=seed)
    cv_results = model_selection.cross_val_score(model, x, y, cv=kfold, scoring=scoring,n_jobs=5)
    results.append(cv_results)
    names.append(name)
    msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())


print(msg)
# boxplot algorithm comparison
fig = plt.figure()
fig.suptitle('Algorithm Comparison')
ax = fig.add_subplot(111)
plt.boxplot(results)
ax.set_xticklabels(names)
plt.show() 
