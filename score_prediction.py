import pandas as pd
import warnings
import statsmodels.api as sm
import pandas as pd
from sklearn import datasets, linear_model
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt
import numpy as np
from datetime import timedelta, date
from time import ctime
import sys

warnings.simplefilter("ignore", DeprecationWarning)
warnings.filterwarnings("ignore")



def prepare_boxscore_with_date_and_win_format():
    df = pd.read_csv('~/git/nba_analysis/files/game_info/appended_info_line_score.tsv', sep='\t')
    df['GAME_DATE_EST'] = pd.to_datetime(df['GAME_DATE_EST'])
    df = df.drop_duplicates()
    df.rename(columns={'PTS': 'TEAM_PTS'}, inplace=True)

    bx = pd.read_csv('/home/nachi/git/nba_analysis/files/boxscores/appended_boxscores.tsv', sep='\t')
    bx = bx.drop_duplicates()
    bx = convert_minutes_to_float(bx)

    #     del bx['Sec_Float']
    merged = bx.merge(df[['GAME_DATE_EST', 'GAME_ID', 'Win', 'TEAM_ID', 'TEAM_PTS']], how='inner',
                      left_on=['GAME_ID', 'TEAM_ID'], right_on=['GAME_ID', 'TEAM_ID'])
    merged.drop_duplicates(inplace=True)
    merged = merged.join(merged.groupby(['GAME_ID', 'TEAM_ID'])['Min_Float'].sum(), on=['GAME_ID', 'TEAM_ID'],
                         rsuffix='_team_total')
    merged['minute_fraction'] = merged['Min_Float'] / merged['Min_Float_team_total']
    return merged


def convert_minutes_to_float(merge):
    merge['Min_Float'] = merge[merge['MIN'].isnull() == False]['MIN'].apply(lambda x: float(x.split(':')[0]))
    merge['Sec_Float'] = merge[merge['MIN'].isnull() == False]['MIN'].apply(lambda x: float(x.split(':')[1]) / 60)
    merge['Total_Minutes_Float'] = merge['Min_Float'] + merge['Sec_Float']
    return merge


def get_df_according_to_dates(start_date,end_date):
    df = prepare_boxscore_with_date_and_win_format()
    df = df.drop_duplicates()

    df_line = pd.read_csv('~/git/nba_analysis/files/game_info/appended_info_line_score.tsv', sep='\t')
    m = df_line.merge(df_line, how='left', left_on='GAME_ID', right_on='GAME_ID', suffixes=['_home', '_away'])
    m = m[(m['TEAM_ID_home'] != m['TEAM_ID_away'])].drop_duplicates()
    m = m[['GAME_ID', 'TEAM_ID_home', 'TEAM_ID_away']]

    df = df.merge(m, how='inner', left_on='GAME_ID', right_on='GAME_ID')
    df = df[(df['TEAM_ID'] == df['TEAM_ID_home'])]

    df['isStarter'] = 1 - df['START_POSITION'].isnull().astype(int)

    # filter_end_date = '2018-11-17'
    # filter_start_date = '2018-10-15'
    df = df[(df['GAME_DATE_EST'] < end_date) & (df['GAME_DATE_EST'] >= start_date)]
    return df


def prepare_set(parameter, name, games_back, df):
    # print  str(ctime()) , 'prepare set ---->'
    list_of_columns = [parameter]

    p = df[df['PLAYER_NAME'] == name]
    p = p.sort_values('GAME_DATE_EST')
    p['days_rest_from_prev_game'] = p['GAME_DATE_EST'].diff().dt.days.fillna(0)
    p = prepare_x_variable(list_of_columns, p, games_back, parameter , df)
    y = p[parameter]
    list_of_columns.remove(parameter)
    x = p[list_of_columns]
    # print str(ctime()), 'prepare set <----'
    return x, y

def prepare_x_variable(list_of_columns,p, games_back,parameter ,df ):
    # print str(ctime()), 'prepare_x_variable ---->'
    p = p[1:]

    for i in range(1, games_back+1):
        p['shift_' + str(i)] = p[parameter].shift(i).fillna(0)
        list_of_columns.append('shift_' + str(i))
        p['shift_' + str(i)] = (p['shift_' + str(i)] - p['shift_' + str(i)].mean()) / p['shift_' + str(i)].std()
    for i in range(1, games_back + 1):
        p['shift_win_' + str(i)] = p['Win'].shift(i).fillna(0)
        list_of_columns.append('shift_win_' + str(i))
        p['shift_win_' + str(i)] = (p['shift_win_' + str(i)] - p['shift_win_' + str(i)].mean()) / p['shift_win_' + str(i)].std()
    for i in range(1, games_back + 1):
        p['shift_TEAM_PTS_' + str(i)] = p['TEAM_PTS'].shift(i).fillna(0)
        list_of_columns.append('shift_TEAM_PTS_' + str(i))
        p['shift_TEAM_PTS_' + str(i)] = (p['shift_TEAM_PTS_' + str(i)] - p['shift_TEAM_PTS_' + str(i)].mean()) / p['shift_TEAM_PTS_' + str(i)].std()
    for i in range(1, games_back + 1):
        p['shift_BLK_' + str(i)] = p['BLK'].shift(i).fillna(0)
        list_of_columns.append('shift_BLK_' + str(i))
        p['shift_BLK_' + str(i)] = (p['shift_BLK_' + str(i)] - p['shift_BLK_' + str(i)].mean()) / p['shift_BLK_' + str(i)].std()
    for i in range(1, games_back + 1):
        p['shift_PTS_' + str(i)] = p['PTS'].shift(i).fillna(0)
        list_of_columns.append('shift_PTS_' + str(i))
        p['shift_PTS_' + str(i)] = (p['shift_PTS_' + str(i)] - p['shift_PTS_' + str(i)].mean()) / p['shift_PTS_' + str(i)].std()
    for i in range(1, games_back + 1):
        p['shift_TO_' + str(i)] = p['TO'].shift(i).fillna(0)
        list_of_columns.append('shift_TO_' + str(i))
        p['shift_TO_' + str(i)] = (p['shift_TO_' + str(i)] - p['shift_TO_' + str(i)].mean()) / p['shift_TO_' + str(i)].std()
    for i in range(1, games_back + 1):
        p['shift_AST_' + str(i)] = p['AST'].shift(i).fillna(0)
        list_of_columns.append('shift_AST_' + str(i))
        p['shift_AST_' + str(i)] = (p['shift_AST_' + str(i)] - p['shift_AST_' + str(i)].mean()) / p['shift_AST_' + str(i)].std()
    for i in range(1, games_back + 1):
        p['shift_STL_' + str(i)] = p['STL'].shift(i).fillna(0)
        list_of_columns.append('shift_STL_' + str(i))
        p['shift_STL_' + str(i)] = (p['shift_STL_' + str(i)] - p['shift_STL_' + str(i)].mean()) / p['shift_STL_' + str(i)].std()
    for i in range(1, games_back + 1):
        p['shift_FG_PCT_' + str(i)] = p['FG_PCT'].shift(i).fillna(0)
        list_of_columns.append('shift_FG_PCT_' + str(i))
        p['shift_FG_PCT_' + str(i)] = (p['shift_FG_PCT_' + str(i)] - p['shift_FG_PCT_' + str(i)].mean()) / p['shift_FG_PCT_' + str(i)].std()
    for i in range(1, games_back + 1):
        p['shift_FGM_' + str(i)] = p['FGM'].shift(i).fillna(0)
        list_of_columns.append('shift_FGM_' + str(i))
        p['shift_FGM_' + str(i)] = (p['shift_FGM_' + str(i)] - p['shift_FGM_' + str(i)].mean()) / p['shift_FGM_' + str(i)].std()
    for i in range(1, games_back + 1):
        p['shift_FT_PCT_' + str(i)] = p['FT_PCT'].shift(i).fillna(0)
        list_of_columns.append('shift_FT_PCT_' + str(i))
        p['shift_FT_PCT_' + str(i)] = (p['shift_FT_PCT_' + str(i)] - p['shift_FT_PCT_' + str(i)].mean()) / p['shift_FT_PCT_' + str(i)].std()
    for i in range(1, games_back + 1):
        p['shift_FTM_' + str(i)] = p['FTM'].shift(i).fillna(0)
        list_of_columns.append('shift_FTM_' + str(i))
        p['shift_FTM_' + str(i)] = (p['shift_FTM_' + str(i)] - p['shift_FTM_' + str(i)].mean()) / p['shift_FTM_' + str(i)].std()
    for i in range(1, games_back + 1):
        p['shift_isStarter_' + str(i)] = p['isStarter'].shift(i).fillna(0)
        list_of_columns.append('shift_isStarter_' + str(i))
        p['shift_isStarter_' + str(i)] = (p['shift_isStarter_' + str(i)] - p['shift_isStarter_' + str(i)].mean()) / p['shift_isStarter_' + str(i)].std()
    for i in range(1, games_back + 1):
        p['shift_days_rest_from_prev_game_' + str(i)] = p['days_rest_from_prev_game'].shift(i).fillna(0)
        list_of_columns.append('shift_days_rest_from_prev_game_' + str(i))
        p['shift_days_rest_from_prev_game_' + str(i)] = (p['shift_days_rest_from_prev_game_' + str(i)] - p['shift_days_rest_from_prev_game_' + str(i)].mean()) / p['shift_days_rest_from_prev_game_' + str(i)].std()
    for i in range(1, games_back + 1):
        p['shift_minute_fraction_' + str(i)] = p['minute_fraction'].shift(i).fillna(0)
        list_of_columns.append('shift_minute_fraction_' + str(i))
        p['shift_minute_fraction_' + str(i)] = (p['shift_minute_fraction_' + str(i)] - p['shift_minute_fraction_' + str(i)].mean()) / p['shift_minute_fraction_' + str(i)].std()


    #         print p
    # new_index = df.index[-1] + 1
    # tmp_df = df.append(pd.DataFrame(index=[new_index], data=df.tail(1).values, columns=df.columns))

    dummy_current = pd.get_dummies(df['TEAM_ID_away'])

    list_of_columns.extend(dummy_current.columns)
    p = p.merge(dummy_current, how='left', left_index=True, right_index=True)

    # in case that the last row is appended , we needto add dummy values for it.
    team_id = p.iloc[-1]['TEAM_ID_away']
    p.iloc[-1:][str(team_id)] = 1
    # p[-1:].fillna(0,inplace=True)
    p = p[list_of_columns]
    #         p.dropna(inplace=True)
    p.fillna(0, inplace=True)
    # print str(ctime()), 'prepare_x_variable <----'
    return p


def predict(games_back, parameter, df, p_name, opposing_team_id, date_today):
        # print str(ctime()), 'predict ---->'
        x, y = prepare_set(name=p_name, parameter='TEAM_PTS', games_back=games_back, df=df)

        # X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0)
        lm = linear_model.BayesianRidge(tol=0.00001, normalize=True, fit_intercept=True, compute_score=True)
        # lm = linear_model.LinearRegression()
        model = lm.fit(x, y)

        list_of_columns = [parameter]
        df.drop_duplicates(inplace=True)
        p = df[df['PLAYER_NAME'] == p_name]
        minute_fraction = p['minute_fraction'].mean()
        p.sort_values('GAME_DATE_EST', inplace=True)

        new_index = p.index[-1] + 1
        p = p.append(pd.DataFrame(index=[new_index], data=p.tail(1).values, columns=p.columns))

        p.loc[p.index == new_index, ['TEAM_ID_away']] = opposing_team_id
        p.loc[p.index == new_index, ['GAME_DATE_EST']] = pd.to_datetime(date_today)

        p['days_rest_from_prev_game'] = p['GAME_DATE_EST'].diff().dt.days

        p = prepare_x_variable(list_of_columns, p, games_back , parameter , df)
        y = p[parameter]
        list_of_columns.remove(parameter)
        x = p[list_of_columns]
        # print str(ctime()), 'predict <----'
        return pd.to_datetime(date_today), lm.predict(x.iloc[-1].to_frame().T)[0], minute_fraction


def get_overal_predict_per_team(curr_team_abv, opp_team_abv, df, parameter, games_back, exclude_players=[],
                                today_date='2018-11-18'):


    cnt = 0
    overall_score = 0
    minute_fraction_sum = 0
    opp_team_id = df[df['TEAM_ABBREVIATION'] == opp_team_abv]['TEAM_ID'].iloc[0]

    latest_games = df.groupby('TEAM_ABBREVIATION').max()[['GAME_DATE_EST']]
    list_of_players = df[
        (df['TEAM_ABBREVIATION'] == curr_team_abv) & (df['GAME_DATE_EST'] == latest_games.ix[curr_team_abv][0]) & (
                    df['COMMENT'].isnull() == True)&(df['minute_fraction']>0.03)]['PLAYER_NAME'].values
    for player in list_of_players:
        try:
            # print player

            date, score, minute_fraction = predict(games_back, parameter, df, player, opp_team_id, today_date)
            cnt += 1
            overall_score += (score * minute_fraction)
            minute_fraction_sum += minute_fraction
            # print ctime()
            # print str(ctime()) , player, np.round(score, 3), np.round(minute_fraction, 3)
        except Exception as e:
            # print player + ' has error --->0 minute fraction assigned'
            # print e
            continue
    # print '********************************************'
    return overall_score * (1 / minute_fraction_sum), cnt
    # print'********************************************'



def calc_score_for_two_teams(team_abv,opp_team_abv,games_back=5,parameter='TEAM_PTS',today_date='' , df=None,exclude_players=[]):


    # df = get_df_according_to_dates('2018-10-15',today_date)
    # print ctime() , 'get_overal_predict_per_team------------>'
    home_list = get_overal_predict_per_team(curr_team_abv=team_abv, opp_team_abv=opp_team_abv, df=df, parameter=parameter,
                                games_back=games_back, exclude_players=exclude_players, today_date=today_date)
    # print
    # print 'get_overal_predict_per_team<------------'
    # print ctime()
    # print 'get_overal_predict_per_team ------------>'
    away_list = get_overal_predict_per_team(opp_team_abv=team_abv, curr_team_abv=opp_team_abv, df=df, parameter=parameter,
                                games_back=games_back, exclude_players=exclude_players, today_date=today_date)
    # print 'get_overal_predict_per_team <------------'
    return np.round(home_list[0]),np.round(away_list[0])


def get_list_of_games(start_date):
    df = pd.read_csv('~/git/nba_analysis/files/game_info/appended_info_line_score.tsv', sep='\t')

    game_info_df = pd.read_csv('/home/nachi/git/nba_analysis/files/game_info/appended_info.tsv', sep='\t')
    game_info_df = game_info_df.merge(df, how='inner', left_on=['GAME_ID', 'HOME_TEAM_ID'], right_on=['GAME_ID', 'TEAM_ID'])
    game_info_df = game_info_df[['GAME_DATE_EST_x', 'GAME_ID', 'TEAM_ABBREVIATION', 'HOME_TEAM_ID', 'TEAM_WINS_LOSSES', 'VISITOR_TEAM_ID',
             'PTS']]
    game_info_df = game_info_df.merge(df, how='inner', left_on=['GAME_ID', 'VISITOR_TEAM_ID'], right_on=['GAME_ID', 'TEAM_ID'])
    game_info_df = game_info_df[['GAME_DATE_EST_x', 'GAME_ID', 'TEAM_ABBREVIATION_x', 'HOME_TEAM_ID', 'TEAM_WINS_LOSSES_x',
             'TEAM_ABBREVIATION_y', 'VISITOR_TEAM_ID', 'TEAM_WINS_LOSSES_y', 'PTS_x', 'PTS_y']]
    game_info_df.rename(columns= {'GAME_DATE_EST_x':'GAME_DATE_EST',
                                  'TEAM_ABBREVIATION_x':'TEAM_ABBREVIATION_home',
                                  'TEAM_WINS_LOSSES_x':'TEAM_WINS_LOSSES_home',
                                  'TEAM_ABBREVIATION_y': 'TEAM_ABBREVIATION_away',
                                  'TEAM_WINS_LOSSES_y':'TEAM_WINS_LOSSES_away',
                                  'PTS_x':'PTS_home',
                                  'PTS_y': 'PTS_away'},inplace=True)
    game_info_df['GAME_DATE_EST']=pd.to_datetime(game_info_df['GAME_DATE_EST'])
    game_info_df=game_info_df[game_info_df['GAME_DATE_EST']>=start_date]
    return game_info_df

def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)


##########################
###   Main proccess   ####
##########################
BACKFILL=True
if BACKFILL==True:

    start_date = '2018-11-01'
    start_date =  sys.argv[1]
    end_date = '2018-11-21'
    end_date = sys.argv[2]

    for single_date in daterange(pd.to_datetime(start_date), pd.to_datetime(end_date)):
        curr_date =  single_date.strftime("%Y-%m-%d")
        df = get_df_according_to_dates('2018-10-15', curr_date)
        game_info_df = get_list_of_games(curr_date)
        for row in game_info_df.iterrows():
            # print ctime()
            today_date =  str(row[1]['GAME_DATE_EST'].date())
            team_a = row[1]['TEAM_ABBREVIATION_home']
            team_b = row[1]['TEAM_ABBREVIATION_away']

            scores = calc_score_for_two_teams(team_a, team_b, 2, 'TEAM_PTS', today_date , df,[])
            if (((scores[0]>scores[1] ) & (row[1]['PTS_home'] >row[1]['PTS_away']  )) |
                ((scores[0] < scores[1]) & (row[1]['PTS_home'] < row[1]['PTS_away']))):
                print today_date,team_a,team_b,1 , scores[0],scores[1],row[1]['PTS_home'],row[1]['PTS_away']
            else:
                print today_date,team_a,team_b,0, scores[0],scores[1],row[1]['PTS_home'],row[1]['PTS_away']
else:
    today_date = '2018-11-20'
    df = get_df_according_to_dates('2018-10-15', today_date)
    exclude_players = ['Dwight Howard',
                       'Caris LeVert',
                       'OG Anunoby','Norman Powell','C.J. Miles',
                       'Tyler Johnson','Rodney McGruder','Goran Dragic',
                       'Seth Curry','Maurice Harkless']
    print calc_score_for_two_teams('LAC', 'HOU',3,'TEAM_PTS',today_date,df)
    print ctime()
    # print calc_score_for_two_teams('LAC', 'ORL', 5, 'TEAM_PTS', today_date,df)
    # print calc_score_for_two_teams('LAC', 'MIA', 5, 'TEAM_PTS', today_date,df)
    # print calc_score_for_two_teams('LAC', 'NYK', 5, 'TEAM_PTS', today_date,df)
# (122.0, 109.0)
# (117.0, 129.0)
# (113.0, 102.0)
# (117.0, 109.0)
