
from nba_py import player
from nba_py.constants import CURRENT_SEASON
import nba_py
from nba_py import team
from nba_py.player import get_player
from nba_py.constants import *
from nba_py import game
import os
import pandas as pd
from dateutil.relativedelta import relativedelta
from dateutil import parser
from datetime import date, timedelta
# boxscore_summary = game.BoxscoreSummary("0021600457")
# print(boxscore_summary.inactive_players())
# boxscore = game.Boxscore("0021600457")
# print boxscore.player_stats()

HOME_PATH = '~/git/nba_analysis/files/'
RELATIVE_PATH= '/home/nachi/git'
class GetDataNBA(object):
    
    def get_box_scores(self, start_date=None):
        print 'get_box_scores------------>'
#         read previouis file
        full_df= pd.read_csv(RELATIVE_PATH + '/nba_analysis/files/boxscores/appended_boxscores.tsv',sep='\t')
        full_df.drop_duplicates(inplace=True)
        d = pd.read_csv(RELATIVE_PATH + '/nba_analysis/files/game_info/appended_info.tsv',sep='\t')
        d.drop_duplicates(inplace=True)
        x = full_df.merge(d,how='left',left_on='GAME_ID',right_on='GAME_ID')
        if start_date is None:
            max_date_in_file_str = x['GAME_DATE_EST'].max()
            max_date_in_file = parser.parse(max_date_in_file_str)
        else:
            max_date_in_file = start_date
        days_back  =(date.today() - max_date_in_file.date()).days
        month=max_date_in_file.month
        year = max_date_in_file.year
        day = max_date_in_file.day
        
        print 'last date that we checked was ' + str(max_date_in_file)
        print 'days back = ' + str(days_back)
        list_of_previous_games = full_df['GAME_ID'].unique()
        for offset_dates in xrange(0,days_back):
            try:
                print offset_dates
                print 'getting boxscore ...'
                boxscore = nba_py.Scoreboard(month=month, day=day, year=year, league_id='00', offset=offset_dates)
                if parser.parse((boxscore.line_score()['GAME_DATE_EST'][0])) == max_date_in_file:
                    continue
                print 'got boxscore running through list_of_games...'
                list_of_games =  boxscore.line_score()['GAME_ID'].unique()
                for single_game in list_of_games:
                    print 'single_game = ' , single_game
                    if single_game not in list_of_previous_games :
                        boxscore = game.Boxscore(single_game).player_stats()
                        if not os.path.exists(RELATIVE_PATH + '/nba_analysis/files/boxscores/appended_boxscores.tsv'):
                            boxscore.to_csv(RELATIVE_PATH + '/nba_analysis/files/boxscores/appended_boxscores.tsv', mode='a', header=True,sep='\t',index=False)
                        else:
                            boxscore.to_csv(RELATIVE_PATH + '/nba_analysis/files/boxscores/appended_boxscores.tsv', mode='a', header=False,sep='\t',index=False)
            
                        advanced = game.BoxscoreAdvanced(single_game).sql_players_advanced()
                        if not os.path.exists(RELATIVE_PATH + '/nba_analysis/files/boxscores_advanced/appended_boxscores_advanced.tsv'):
                            advanced.to_csv((RELATIVE_PATH + '/nba_analysis/files/boxscores_advanced/appended_boxscores_advanced.tsv'), mode='a', header=True,sep='\t',index=False)
                        else:
                            advanced.to_csv((RELATIVE_PATH + '/nba_analysis/files/boxscores_advanced/appended_boxscores_advanced.tsv'), mode='a', header=False,sep='\t',index=False)
                    else: 
                        print 'single exists   ' + str(offset_dates)
            except Exception as e:
                print e
                continue
        full_df= pd.read_csv(RELATIVE_PATH + '/nba_analysis/files/boxscores/appended_boxscores.tsv',sep='\t')
        full_df = full_df.drop_duplicates()
        full_df.to_csv(RELATIVE_PATH + '/nba_analysis/files/boxscores/appended_boxscores.tsv',sep='\t',index=False)
        
        full_df_ad= pd.read_csv(RELATIVE_PATH + '/nba_analysis/files/boxscores_advanced/appended_boxscores_advanced.tsv',sep='\t')
        full_df_ad = full_df_ad.drop_duplicates()
        full_df_ad.to_csv(RELATIVE_PATH + '/nba_analysis/files/boxscores_advanced/appended_boxscores_advanced.tsv',sep='\t',index=False)
        print 'get_box_scores <------------'
        
    def get_game_logs_based_on_box_score(self,season_list):
        d = pd.read_csv('~/git/nba_analysis/files/boxscores/appended_boxscores.tsv',sep='\t')
        pre_list= pd.read_csv('~/git/nba_analysis/files/nba_analysis_game_logs/appended_info.tsv',sep='\t')
        id_list = d['PLAYER_ID'].unique()
        id_list_pre_list = pre_list['Player_ID'].unique()
        player_counter = 0
        for player_id in id_list:
            player_counter = player_counter + 1
            print player_id
            print " player count is " +str(player_counter) + '  out of ' + str(len(id_list) )
            if player_id not in id_list_pre_list:
                for season in season_list:
                    self.get_game_logs(season,player_id)
        print 'done'   
    def update_player_list(self):
        print 'update_player_list -------->'
        df = pd.read_csv('~/git/personal/files/boxscores/appended_boxscores.tsv',sep='\t')
        df[['PLAYER_ID','PLAYER_NAME']].drop_duplicates().to_csv( HOME_PATH + 'boxscores/player_list.tsv',sep='\t',index=False)
        print 'update_player_list <--------'
        
    def get_players(self,season_list):
        print 'get_players ---->'
        self.update_player_list()
        x = pd.read_csv('~/git/personal/files/boxscores/appended_boxscores.tsv',sep='\t')    
        for row in x.iterrows():
            try:
                if row[1].AlreadyParsed == 0:
                    print row
                    id = row[1].PERSON_ID
                    last_name = row[1].DISPLAY_LAST_COMMA_FIRST.split(',')[0].replace(' ','')
                    first_name=row[1].DISPLAY_LAST_COMMA_FIRST.split(',')[1].replace(' ','')
                    plyr = get_player(str(first_name), str(last_name), just_id=False)
                    
                    # get player info
                    p_sum = player.PlayerSummary(id)
                    df_player_info = p_sum.info()
                    if not os.path.exists('~/git/nba_analysis/files/nba_analysis_info/appended_info.tsv'):
                        df_player_info.to_csv(('~/git/nba_analysis/files/nba_analysis_info/appended_info.tsv'), mode='a', header=True,sep='\t',index=False)
                    else:
                        df_player_info.to_csv(('~/git/nba_analysis/files/nba_analysis_info/appended_info.tsv'), mode='a', header=False,sep='\t',index=False)
                        
                    for season in season_list:
#                         get game logs poer player per season
                        self.get_game_logs(season,id,first_name,last_name)    
                        for key in TEAMS :
                            print TEAMS[key]['id']
                            print season
                                
                            tvp = team.TeamVsPlayer(team_id=TEAMS[key]['id'],
                                        vs_player_id=plyr['PERSON_ID'],
                                        season=season)
                            
                            sa_on = tvp.shot_area_on_court()
                            sa_on['Season']=season
                            if not os.path.exists('~/git/nba_analysis/files/team_vs_player/appended_team_vs_player.tsv'):
                                sa_on.to_csv(('~/git/nba_analysis/files/team_vs_player/appended_team_vs_player.tsv'), mode='a', header=True,sep='\t',index=False)
                            else:
                                sa_on.to_csv(('~/git/nba_analysis/files/team_vs_player/appended_team_vs_player.tsv'), mode='a', header=False,sep='\t',index=False)
            
                            sa_off = tvp.shot_area_off_court()
                            if not os.path.exists('~/git/nba_analysis/files/team_vs_player/appended_team_vs_player.tsv'):
                                sa_off.to_csv(('~/git/nba_analysis/files/team_vs_player/appended_team_vs_player.tsv'), mode='a', header=True,sep='\t',index=False)
                            else:
                                sa_off.to_csv(('~/git/nba_analysis/files/team_vs_player/appended_team_vs_player.tsv'), mode='a', header=False,sep='\t',index=False)
                    print 'updatinig parsed csv'
                    x.loc[x.index == row[0],'AlreadyParsed']=1
                    x.to_csv('~/git/nba_analysis/files/player_list/player_list_file.tsv',sep='\t',index=False)
            
            except :
                continue
        print 'get_players <----'    
    def get_game_logs(self,season,id,first_name=None,last_name=None):
        try:
            print 'get_game_logs----->'
            start_date = parser.parse(season.split('-')[0]+ '-07-01')
            end_date = parser.parse('20'+season.split('-')[1] + '-07-01')
            if not os.path.exists('~/git/nba_analysis/files/nba_analysis_game_logs/appended_info.tsv'):
                game_logs = player.PlayerGameLogs(id,season=season).info()
                game_logs.to_csv(('~/git/nba_analysis/files/nba_analysis_game_logs/appended_info.tsv'), mode='a', header=True,sep='\t',index=False)
            else:
                tmp = pd.read_csv('~/git/nba_analysis/files/nba_analysis_game_logs/appended_info.tsv',sep='\t')
                tmp['GAME_DATE']= pd.to_datetime(tmp['GAME_DATE'])
                if len(tmp[(tmp['Player_ID']==id) ])==0:
#                 if len(tmp[(tmp['Player_ID']==id)& (tmp['GAME_DATE'] >= str(start_date)[:10]) &(tmp['GAME_DATE'] < str(end_date)[:10]) ])==0:
                    game_logs = player.PlayerGameLogs(id,season=season).info()
                    game_logs.to_csv(('~/git/nba_analysis/files/nba_analysis_game_logs/appended_info.tsv'), mode='a', header=False,sep='\t',index=False)
                else:
                    print 'player already has data'
        except:
            print 'player didnt play in that year'
            print season
        print 'get_game_logs <-----'
    def get_game_info(self):
        print 'get_game_info ---->'
        relative_path = '/home/nachi/git'
        bx = pd.read_csv(relative_path + '/nba_analysis/files/boxscores/appended_boxscores.tsv',sep='\t')
        bx.drop_duplicates(inplace=True)
        
        list_of_games = bx['GAME_ID'].unique()
        for game_id  in list_of_games:  
            if  os.path.exists(relative_path +'/nba_analysis/files/game_info/appended_info.tsv'):
                games_df = pd.read_csv(relative_path + '/nba_analysis/files/game_info/appended_info.tsv',sep='\t')
                list_of_game_ids  = games_df['GAME_ID'].unique()
            else:
                list_of_game_ids= []
            if game_id not in list_of_game_ids:
                print game_id
                try:
                    summary = game.BoxscoreSummary('00' + str(game_id)).game_summary()
                    if not os.path.exists(relative_path + '/nba_analysis/files/game_info/appended_info.tsv'):
                        summary.to_csv((relative_path + '/nba_analysis/files/game_info/appended_info.tsv'), mode='a', header=True,sep='\t',index=False)
                    else:
                        summary.to_csv((relative_path + '/nba_analysis/files/game_info/appended_info.tsv'), mode='a', header=False,sep='\t',index=False)
                except Exception as e:
                    print e
        print 'get_game_info <----'

    def get_game_info_per_game_id(self):
        print('to do...')
    def get_line_score(self):
        print 'get_line_score ---- >'
        bx = pd.read_csv(RELATIVE_PATH + '/nba_analysis/files/boxscores/appended_boxscores.tsv',sep='\t')
        print 'dropping duplicates...'
        bx.drop_duplicates(inplace=True)
        print 'running through ids'
        list_of_games = bx['GAME_ID'].unique()
        for game_id  in list_of_games:  
            if  os.path.exists(RELATIVE_PATH +'/nba_analysis/files/game_info/appended_info_line_score.tsv'):
                games_df = pd.read_csv(RELATIVE_PATH + '/nba_analysis/files/game_info/appended_info_line_score.tsv',sep='\t')
                list_of_game_ids  = games_df['GAME_ID'].unique()
            else:
                list_of_game_ids= []
            if game_id not in list_of_game_ids:
                try:
                    print game_id
                    summary = game.BoxscoreSummary('00' + str(game_id)).line_score()
                    summary['Win'] = None
                    summary.loc[summary['PTS'].max()==summary['PTS'],'Win']=1
                    summary.loc[summary['PTS'].min()==summary['PTS'],'Win']=0
                    if not os.path.exists(RELATIVE_PATH + '/nba_analysis/files/game_info/appended_info_line_score.tsv'):
                        summary.to_csv((RELATIVE_PATH +'/nba_analysis/files/game_info/appended_info_line_score.tsv'), mode='a', header=True,sep='\t',index=False)
                    else:
                        summary.to_csv((RELATIVE_PATH + '/nba_analysis/files/game_info/appended_info_line_score.tsv'), mode='a', header=False,sep='\t',index=False)
                except Exception as e:
                    print e
            else: 
                print 'game_id in list: ' + str(game_id)
        print 'get_line_score <---- '
    
    def convert_date_to_season(self,date):
        
        if datetime.now().month > 6:
            SEASON = str(_last_year) + "-" + str(_last_year + 1)[2:]
        else:
            SEASON = str(_curr_year - 1) + "-" + str(_curr_year)[2:] 
        return SEASON    
            
    def get_rosters(self,team_id,date):
        _last_year = datetime.now().year -1
        season = self.convert_date_to_season(_last_year) 
        TeamCommonRoster(str(team_id),season=season).roster() 
           
if __name__ == '__main__':
    import sys
    function_name       = sys.argv[1]
    data_ex             = GetDataNBA()
    

    if function_name == 'get_players':
        data_ex.get_players(['2013-14','2014-15','2015-16','2016-17','2017-18'])
    elif function_name == 'get_game_info':
        data_ex.get_game_info()
    elif function_name =='get_box_scores':
        data_ex.get_box_scores()
    elif function_name =='get_line_score':
        data_ex.get_line_score()
    elif function_name =='get_all':
        try:
            data_ex.get_box_scores()
        except Exception as e:
            print(e)
        try:
            data_ex.get_game_info()
        except Exception as e:
            print(e)
        try:
            data_ex.get_line_score()
        except Exception as e:
            print(e)

    elif function_name =='get_game_logs_based_on_box_score':
        data_ex.get_game_logs_based_on_box_score(['2013-14','2014-15','2015-16','2016-17','2017-18'])
        
# https:/programtalk.com/vs2/?source=python/11746/nba_py/examples/example_team.py#
