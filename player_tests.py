
from nba_py import player
from nba_py.constants import CURRENT_SEASON
import nba_py
from nba_py import team
from nba_py.player import get_player
from nba_py.constants import *
from nba_py import game
import os
import pandas as pd
import pandas as pd
import matplotlib.pyplot as plt 
from pandas.stats.api import ols
import statsmodels.api as sm
import numpy as np
import datetime as dt
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import confusion_matrix, f1_score
from sklearn import metrics
from sklearn.cross_validation import KFold


class PlayerTestsNBA(object):
    def __init__(self):
        print 'init...'
    def get_minutes(self,time_str):
        if time_str == None:
            return None
        minutes, sec = time_str.split(':')
        return float(minutes) + float(float(sec)/60)
    #################################################
    #     Description:
    #     This function tests the correlation 
    #     between the amount of time in the league and
    #     the production (plus-minus) on the court.
    #################################################
    def check_trend_plus_minus(self):
    
        bx = pd.read_csv('~//git/personal/files/boxscores/appended_boxscores.tsv',sep='\t')
        bx=bx.drop_duplicates()
        print bx.columns
        list_of_player_ids=bx['PLAYER_ID'].unique()
        bx_info = pd.read_csv('~//git/personal/files/game_info/appended_info.tsv',sep='\t')
        bx_info.drop_duplicates(inplace=True)
        bx_info['GAME_DATE_EST']=pd.to_datetime(bx_info['GAME_DATE_EST'])
        bx_info_cut = bx_info[['GAME_DATE_EST','GAME_ID','HOME_TEAM_ID','VISITOR_TEAM_ID','SEASON']]
        
        merge = bx.merge(bx_info_cut,how='inner',left_on='GAME_ID' , right_on='GAME_ID')
        df = self.convert_minutes_to_float(merge)
        appended_df_with_datediff = pd.DataFrame(columns=df.columns)
        players = df['PLAYER_ID'].unique()

        for player in players:
            tmp_df = df[df['PLAYER_ID']==team]
            tmp_df=tmp_df.sort_values('GAME_DATE_EST')
            tmp_df['day_diff'] = tmp_df['GAME_DATE_EST'].diff()
            tmp_df = tmp_df[1:]
            tmp_df['day_diff'] = tmp_df['day_diff'].dt.days
            appended_df_with_datediff = appended_df_with_datediff.append(tmp_df)
    
    def convert_minutes_to_float(self,merge):
        merge['Min_Float'] = merge[merge['MIN'].isnull()==False]['MIN'].apply(lambda x: float(x.split(':')[0]))
        merge['Sec_Float'] = merge[merge['MIN'].isnull()==False]['MIN'].apply(lambda x: float(x.split(':')[1])/60)
        merge['Total_Minutes_Float'] = merge['Min_Float'] + merge['Sec_Float']    
        return merge
    
    
    
    
    def test_combo_prev_win_and_day_rest(self):    
        df_rest = self.check_one_day_rest_vs_none(False)
        df_after_win = self.check_game_after_win(False)
        df_home_away = self.check_home_games_vs_away(False)
        
        merged = df_rest.merge(df_after_win,how='inner',left_on=['GAME_ID','TEAM_ID'],right_on=['GAME_ID','TEAM_ID'])
        merged=merged.merge(df_home_away,how='inner',left_on=['GAME_ID','TEAM_ID'],right_on=['GAME_ID','TEAM_ID'])
        merged['log_day_diff']=np.log(merged['day_diff'])
        
        
        X=merged[['day_diff','Win_diff','IsHome']]
        Y = merged['Win']
        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, random_state=1)
        
        results_logit = sm.Logit(y_train,sm.add_constant(X_train)).fit()
        results_probit=  sm.Probit(y_train,sm.add_constant(X_train)).fit()
        results_ols =   sm.OLS(y_train,sm.add_constant(X_train)).fit()
        
        
        results = results_logit
        print results.summary()
        
        Y_hat = pd.DataFrame(results.predict(sm.add_constant(X_test)))
        y_test = pd.DataFrame(y_test)
        validation_matrix = Y_hat.merge(y_test,how='inner',left_index=True,right_index=True)
        validation_matrix.columns=['Y_hat','Y']
        
#         check success rate
        tmp = validation_matrix.copy()
        tmp.loc[tmp['Y_hat']<0.5,'Y_hat']=0
        tmp.loc[tmp['Y_hat']>0.5,'Y_hat']=1
        print float(len(tmp[tmp['Y_hat']==tmp['Y']])/float(len(tmp)))
        
        
        
        

    def run_classifiers(self,X_train,y_train,X_test,y_test):
        
        clf = GaussianNB()
        clf.fit(X_train, y_train)
        clf.predict(X_test)
        print '************'
        print 'Naive Bayes: '
        print metrics.accuracy_score(y_test, clf.predict(X_test))
        print '************'
        
        from sklearn.svm import SVC
        clf = SVC(probability=True)
        clf.fit(X_train, y_train)
        clf.predict(X_test)
        print '************'
        print 'SVC : '
        print metrics.accuracy_score(y_test, clf.predict(X_test))
        print '************'
        
        
        from sklearn.ensemble import RandomForestClassifier as RF
        clf = RF()
        clf.fit(X_train, y_train)
        clf.predict(X_test)
        print '************'
        print 'Random Forrest: '
        print metrics.accuracy_score(y_test, clf.predict(X_test))
        print '************'
        
        from sklearn.neighbors import KNeighborsClassifier as KNN
        clf = KNN()
        clf.fit(X_train, y_train)
        clf.predict(X_test)
        print '************'
        print 'KNN: '
        print metrics.accuracy_score(y_test, clf.predict(X_test))
        print '************'
        
    
    def explore_team_vs_player(self,team_id,player_id):
        print 'explore_team_vs_player'
        
    
        
        
if __name__ == '__main__':
    import sys
    try:
        function_name       = sys.argv[1]
    except:
        function_name       = 'check_trend_plus_minus'
    data_test = PlayerTestsNBA()
    
    

    if function_name == 'check_trend_plus_minus':
        data_test.check_trend_plus_minus()
    
    if function_name == 'explore_team_vs_player':
        data_test.explore_team_vs_player()
# https://programtalk.com/vs2/?source=python/11746/nba_py/examples/example_team.py#
